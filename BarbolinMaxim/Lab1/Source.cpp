#include<iostream>
using namespace std;

int main() {
	cout << "Exercise 1." << endl;
	float ResInFloat, af = 1000, bf = 0.0001;
	double ResInDouble, ad = 1000, bd = 0.0001;
	ResInFloat = (pow(af + bf, 2) - (pow(af, 2) + 2 * af*bf)) / pow(bf, 2);
	ResInDouble = (pow(ad + bd, 2) - (pow(ad, 2) + 2 * ad*bd)) / pow(bd, 2);
	cout << "Result in float type : " << ResInFloat << "\nResult in double type : " << ResInDouble << endl;

	cout << "Exercise 2. Here 0 equals false and 1 eq true" << endl;
	int n = 3, m = 3, z;
	cout << "n+++m :" << n++ + m << endl;
	z = m-- > n; 
	cout << "n-- > m = " << z << endl;
	z = n-- > m; 
	cout << "n-- > m = " << z << endl;
	system("pause");
}