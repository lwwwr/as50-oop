#pragma once
#include <iostream>
class Tasks {
private:
	int size;
	static int MAX_SIZE;
	char *pMas;
	void add(char elem);
public:
	Tasks();
	Tasks(const Tasks&);
	Tasks(const char*, int);
	Tasks& operator + (char);
	Tasks& operator + (const Tasks&);
	bool operator == (const Tasks&);
	~Tasks();
	void inPut();
	void print();
};
int Tasks::MAX_SIZE = 225;
Tasks::Tasks() : pMas(nullptr), size(0) {}
Tasks::Tasks(const Tasks &other) : Tasks(other.pMas, other.size) {}
Tasks::Tasks(const char* pMas, int size) {
	if (this->pMas) delete[] this->pMas;
	this->pMas = new char[size];
	this->size = size;
	for (int i = 0; i < size; i++)
		this->pMas[i] = pMas[i];
}
Tasks::~Tasks() { delete[] pMas; }
void Tasks::add(char elem) {
	char *newMas = new char[size + 1];
	bool check = true;
	for (int i = 0; i < size; i++) {
		if (pMas[i] == elem) check = false;
		newMas[i] = pMas[i];
	}
	if(check) newMas[size++] = elem;
	delete[] pMas;
	pMas = newMas;
}
Tasks& Tasks::operator + (char elem) {
	Tasks *newObj = new Tasks(*this);
	newObj->add(elem);
	return *newObj;
}
Tasks& Tasks::operator + (const Tasks& other) {
	Tasks *newObj = new Tasks(*this);
	for (int i = 0; i < other.size; i++)
		newObj->add(other.pMas[i]);
	return *newObj;
}
bool Tasks::operator == (const Tasks& other) {
	if (size != other.size) return false;
	bool itsRavno = false;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < other.size; j++)
			if (pMas[i] == other.pMas[j]) {
				itsRavno = true;
				break;
			}
		if (!itsRavno) return false;
		else itsRavno = false;
	}
	return true;
}
void Tasks::inPut() {
	char elem;
	while (std::cin >> elem && size < MAX_SIZE) {
		if (elem == '.') return;
		add(elem);
	}
}
void Tasks::print() {
	for (int i = 0; i < size; i++)
		std::cout << pMas[i] << " ";
	std::cout << std::endl;
}