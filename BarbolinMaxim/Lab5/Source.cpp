#include <iostream>
#include <QVector>
//__________________________________________
void ex1() {
	using std::cout;
	using std::endl;
	cout << "Exercise1\n------------------------\n";
	QVector<int> vec1 = { 0, 1, 5, 0, 4, 8, -3, 0 - 10, 0, 12, 0, 0, 3 };
	QVector<int> vec2;
	int elem;
	foreach(elem, vec1)
	 if (elem != 0) vec2 += elem;
	cout << "input array: ";
	foreach(elem, vec1) cout << elem << " ";
	cout << endl << "sorted array: ";
	foreach(elem, vec2) cout << elem << " ";
	
}
//__________________________________________
void ex2() {
	using std::cout;
	using std::endl;
	cout << endl << endl << "\n\nExercise2\n------------------------\n";
	QVector<int> vec1 = { 1, 2, 3, 5, 10, 4 };
	int elem, max, min, a, b;
	int rez = 1;
	if (vec1.isEmpty()) {
		cout << "vector is empty";
		return;
	}
	max = min = vec1.first();
	foreach(elem, vec1){
		if (max < elem) max = elem; 
		if (min > elem) min = elem; 	
	}
	if (vec1.indexOf(max) < vec1.indexOf(min)) {
		a = vec1.indexOf(max); 
		b = vec1.indexOf(min);	
	}
	else {
		a = vec1.indexOf(min);
		b = vec1.indexOf(max);	
	}
	for (a; a < b; a++) rezult *= vec1[a]; 
	cout << "input array: ";
	foreach(elem, vec1) cout << elem << " ";
	cout << "\nAns: " << rez;
}
//__________________________________________
int main() {
	ex1();
	ex2();
	return 0;	
}