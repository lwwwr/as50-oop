#pragma once
#include "Persona.h"
class HeadOfDepartment :
	public Persona
{
protected: string aNameOfDepartment;
public:
	HeadOfDepartment();
	HeadOfDepartment(const HeadOfDepartment&);
	HeadOfDepartment(int, string, string);
	~HeadOfDepartment();
	void Show() override;
};
HeadOfDepartment::HeadOfDepartment() : HeadOfDepartment(0, "???", "???") {}
HeadOfDepartment::HeadOfDepartment(int age, string name, string nameDep) : Persona(age, name), aNameOfDepartment(nameDep) {}
HeadOfDepartment::HeadOfDepartment(const HeadOfDepartment &oth) : HeadOfDepartment(oth.aAge, oth.aName, oth.aNameOfDepartment) {}
void HeadOfDepartment::Show() {
	cout << "head of departament ->\nAge: " << aAge
		<< "\nName: " << aName << "\ndepartament: " << aNameOfDepartment << endl << endl;
}
HeadOfDepartment::~HeadOfDepartment() {}