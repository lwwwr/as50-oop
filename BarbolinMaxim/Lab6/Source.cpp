﻿#include "Persona.h"
#include "HeadOfDepartment.h"
#include "Teacher.h"
#include "Student.h"

int main() {
	Student a(18, "Maxim", "FEIS", 2);
	Student b(a);
	HeadOfDepartment c(35, "Ostapchik A.S.", "math");
	Teacher d(53, "Dambldor M.W.", "foreign language");
	a.Push();
	b.Push();
	c.Push();
	d.Push();
	PersonList::ShowList();
	system("pause");
}