#pragma once
#include "Persona.h"
class Teacher : 
	public Persona{
	protected: string aAcademicSubject;
		public:
			Teacher();
			Teacher(const Teacher&);
			Teacher(int, string, string);
			~Teacher();
			void Show() override;
			};
Teacher::Teacher() : Teacher(0, "???", "???") {}
Teacher::Teacher(int age, string name, string academicSubject) : Persona(age, name), aAcademicSubject(academicSubject) {}
Teacher::Teacher(const Teacher &oth) : Teacher(oth.aAge, oth.aName, oth.aAcademicSubject) {}
void Teacher::Show() {
	cout << "Teacher ->\nAge: " << aAge  << "\nName: " << aName << "\nAcademic subject: " << aAcademicSubject << endl << endl;
}
Teacher::~Teacher() {}