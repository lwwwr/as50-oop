﻿// 4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <istream>
using namespace std;

void a(char *sin, char *sout, int size);
void b(ifstream& z, ofstream& x, int bs);
void a(char *sin, char *sout, int size)
{
	for (int i = 0; i < size - 1; i++)
		sout[i] = sin[size - 2 - i];
	sout[size - 1] = 0;
}
void b(ifstream& z, ofstream& x, int bs)
{
	char *str1 = new char[bs];
	char *str2 = new char[bs];
	if (z && x)
	{
		z.seekg(-bs - 1, ios::end);
		while (z.tellg() ) {
			z.get(str1, bs);
			a(str1, str2, bs);
			x << str2;
			if (z.tellg() <= bs * 2 - 2) {
				int b = (int)z.tellg() - bs - 2;
				z.seekg(0, ios::beg);
				z.get(str1, b);
				a(str1, str2, z.tellg());
				x << str2;
				break;
			}
			else
				z.seekg(-(bs * 2 - 2), ios::cur);
		}
	}
	else cerr << "File not open";
	cout << endl;	
	z.close();
	x.close();
}
int main()
{
	ifstream input("Lab4in.txt");
	ofstream output("Lab4out.txt", ios::ate);
	b(input, output, 512);
	return 0;
}
