﻿// ConsoleApplication121.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

struct z
{
	char a;
	z *next;
};

class list
{
public:
	list();
	void add(char c);
	explicit list(char c);
	void operator+(char c);
	void operator--(int i);
	bool operator != (list &l);
	void print();
	~list();

private:
	z *head;
	int znach;
};

void list::print()
{
	if (!head) std::cout << "List is empty\n";
	else
	{
		z *h, *t;
		h = head;
		do
		{
			std::cout << h->a << "<-";
			t = h->next;
			h = t;
		} while (h);
		std::cout << "\n";
	}
}

void list::add(char c)
{
	if (head == NULL)
	{
		head = new z;
		head->a = c;
		head->next = NULL;
		znach = 1;
	}
	else
	{
		z *h;
		h = new z;
		h->a = c;
		h->next = head;
		head = h;
		znach++;
	}
}

list::list()
{
	head = NULL;
	znach = 0;
}

list::list(char c)
{
	head = new z;
	head->a = c;
	head->next = NULL;
	znach = 1;
}

void list::operator+(char c)
{
	add(c);
}

void list::operator--(int i)
{
	if (!head->next)
	{
		z *h;
		h = head;
		head = h->next;
		delete h;
		znach--;
	}
	else std::cout << "List is empty\n";
}

list::~list()
{
	z *h;
	if (head)
	{
		do
		{
			h = head;
			head = h->next;
			delete h;
		} while (head);
	}
}

bool list::operator != (list &l)
{
	if ((!head) || (!l.head)) std::cout << "Some List is empty\n";
	else
	{
		if (znach != l.znach) return true;
	}
	return false;
}


int main()
{
	list d1, d2('s');
	d1.print();
	d2.print();
	d1 + 'q';
	d2 + 'u';
	d1.print();
	d2.print();
	if (d1 != d2) std::cout << "Not equal\n";
	else std::cout << "Are equal\n";
}
