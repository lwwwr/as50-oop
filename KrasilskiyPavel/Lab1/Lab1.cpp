﻿// Lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

int main() {
	double dA = 100, dB = 0.001;
	float fA = 100, fB = 0.001;
	float z[11]; 
	double k[11];

	z[1] = fA - fB;
	z[2] = pow(z[1], 4); 
	z[3] = pow(fA, 4); 
	z[4] = 4 * pow(fA, 3) * fB; 
	z[5] = 6 * pow(fA, 2) * pow(fB, 2); 
	z[6] = z[3] - z[4] + z[5]; 
	z[7] = z[2] - z[6]; 
	z[8] = pow(fB, 4); 
	z[9] = 4 * fA * pow(fB, 3); 
	z[10] = z[8] - z[9]; 
	z[0] = z[7] / z[10]; 
	k[1] = dA - dB;
	k[2] = pow(k[1], 4); 
	k[3] = pow(dA, 4); 
	k[4] = 4 * pow(dA, 3) * dB; 
	k[5] = 6 * pow(dA, 2) * pow(dB, 2); 
	k[6] = k[3] - k[4] + k[5]; 
	k[7] = k[2] - k[6]; 
	k[8] = pow(dB, 4); 
	k[9] = 4 * dA * pow(dB, 3); 
	k[10] = k[8] - k[9]; 
	k[0] = k[7] / k[10]; 
	cout << z[0] << endl << k[0] << endl;
	int n = 20, m = 10;
	int a = n-- - m;
	int b = m--<n;
	int c = n++>m;
	cout << a << endl << b << endl << c << endl;
	system("pause");
	return 0;
}
