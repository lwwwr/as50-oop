﻿#pragma once
#include "product.h"
class milkProduct : public product {
protected:
	string aTypeOfProduct;
public:
	milkProduct(string, int, string);
	milkProduct(milkProduct&);
	milkProduct();
	~milkProduct();
	void view() override;
};

milkProduct::milkProduct(string name, int shelfLife, string type) : product(name, aShelfLife), aTypeOfProduct(type) {}
milkProduct::milkProduct() : milkProduct("NO SET", 0, "NO SET") {}
milkProduct::milkProduct(milkProduct &obj) : milkProduct(obj.aName, obj.aShelfLife, obj.aTypeOfProduct) {}
milkProduct::~milkProduct() {}
void milkProduct::view() {
	cout << setw(20) << left << "milkProduct" << setw(20) << left << aName << setw(10) << left <<
		aShelfLife << setw(20) << left << aTypeOfProduct << setw(20) << left << "-------" << endl;
}

