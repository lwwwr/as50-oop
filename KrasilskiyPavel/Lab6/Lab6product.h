﻿#pragma once
#include "goods.h"
class product : public goods{
protected:
	int aShelfLife;
public:
	product(string, int);
	product(product&);
	product();
	~product();
	void view() override;
};

product::product(string name, int shelfLife) : goods(name), aShelfLife(shelfLife) {}
product::product() : product("NO SET", 0) {}
product::product(product &obj) : product(obj.aName, obj.aShelfLife) {}
product::~product() {}
void product::view() {
	cout << setw(20) << left << "product" << setw(20) << left << aName << setw(10) << left <<
		aShelfLife << setw(20) << left << "--------" << setw(20) << left << "-------" << endl;
}
