﻿// lab6.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "goods.h"
#include "milkProduct.h"
#include "product.h"
#include "toy.h"

int main() {
	toy t("robot", "plastic");
	milkProduct milk("burenka", 10, "moloko");
	product prod ("alenka", 50);
	t.add();
	milk.add();
	prod.add();
	goods::viewAll();
}