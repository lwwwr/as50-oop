﻿// Lab6Oblast.cpp : Defines the entry point for the console application.
//

#pragma once
 #include <iostream>
#include <string>
class Oblast {
	protected:
		std::string nameobl;
		std::string people;
		std::string maincity;
		public:
			Oblast();
			Oblast(std::string,std::string,std::string );
			Oblast(const Oblast&);
			virtual void Show() = 0;
			virtual void Add() = 0;
			~Oblast();
			
};
Oblast::Oblast() {
	nameobl = "UndefinedNameofOblast";
	people = "UndefinedPeople";
maincity = "UndefinedMainofcity";
}
Oblast::Oblast(std::string _nameobl, std::string _people, std::string _maincity) {
	nameobl = _nameobl;
	people = _people;
	maincity = _maincity;
};
Oblast::Oblast(const Oblast& _Oblast) { };
Oblast::~Oblast() { };