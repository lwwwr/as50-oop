﻿#pragma once
#include <iostream>
#include <string>
#include <iomanip>

using std::cout;
using std::endl;
using std::string;
using std::setw;
using std::left;

class goods {
private:
	static goods *apHead;
	static goods *apTail;
	goods *apNext = nullptr;
protected:
	string aName;
public:
	goods(string);
	goods(goods&);
	goods();
	~goods();
	virtual void view() = 0;
	void add();
	static void viewAll();
};

goods* goods::apHead = nullptr;
goods* goods::apTail = nullptr;

goods::goods(string name) : aName(name) {}
goods::goods() : goods("NO SET") {}
goods::goods(goods &obj) : goods(obj.aName) {}
goods::~goods() {}

void goods::add() {
	if (!apHead) apHead = apTail = this;
	else {
		apTail->apNext = this;
		apTail = apTail->apNext;
	}
}
void goods::viewAll() {
	cout << setw(20) << left << "Type of goods" << setw(20) << left << "NAME" << setw(10) << left <<
		"SHEL LIFE" << setw(20) << left << "TYPE OF PRODUCT" << setw(20) << left << "MATERIAL" << endl;
	for (int i = 0; i < 78; i++)cout << "-";
	cout << endl;
	for (goods *pCur = apHead; pCur; pCur = pCur->apNext)
		pCur->view();
}