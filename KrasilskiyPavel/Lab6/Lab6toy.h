﻿#pragma once
#include "goods.h"
class toy :
	public goods {
protected:
	string aMaterial;
public:
	toy(string, string);
	toy(toy&);
	toy();
	~toy();
	void view() override;
};

toy::toy(string name, string material) : goods(name), aMaterial(material) {}
toy::toy() : toy("NO SET", "NO SET") {}
toy::toy(toy &obj) : toy(obj.aName, obj.aMaterial) {}
toy::~toy() {}
void toy::view() {
	cout << setw(20) << left << "toy" << setw(20) << left << aName << setw(10) << left <<
		"-------" << setw(20) << left << "--------" << setw(20) << left << aMaterial << endl;
}


