#include <QVector>
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

void task1();
void task2();

int main() {
    task1();
    task2();
    return 0;
}

void task1(){
    cout << "task1" << endl << endl;
    QVector <int> arr = {1, 3, 2, 4, 3, 5, 4, 6}, rezult, even;
    for(int i = 0; i < arr.size(); ++i)
        if(i % 2 == 0)
            even += arr[i];
        else
             rezult += arr[i];
    rezult += even;
    cout << "initial: ";
    foreach(int el, arr)
        cout << el << " ";
    cout << endl << endl << "rezult: ";
    foreach(int el, rezult)
        cout << el << " ";
    cout << endl << endl << "=====================" << endl << endl;
}

void task2(){
    cout << "task2 " << endl << endl;
    QVector <int> arr = {1, 3, 2, 4, 3, -5, 4, 6};
    int rez = 0;
    QVectorIterator<int> i(arr);
    while(i.hasNext()){
        if(i.peekNext() < 0) break;
        else i.next();
    }
    cout << "initial: ";
    foreach(int el, arr)
        cout << el << " ";
    while(i.hasNext())
        rez += i.next();
    cout << endl << endl << "rezult: " << rez;
}
