#include <QtCore/QCoreApplication>
#include <QString> 
#include <QTextStream> 
#include <iostream> 
using namespace std;

QTextStream in(stdin);

QString sort(QString str)
{
	QString letters,numbers;
	QStringList items = str.split("");
	for (int i = 0; i < items.count(); i++)
	{
		if (items[i] == "0"|| items[i].toInt())
			numbers += items[i];
		else
			letters += items[i];
	}
	return letters + numbers;
}

int main()
{
	QString Yourstr;
	cout << "Input your string: "<< endl;
	Yourstr = in.readLine();
	cout << "Output of processed string: " << endl << sort(Yourstr).toStdString() << endl;
}