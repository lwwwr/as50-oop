﻿#include "pch.h"
#include<stdio.h>
#include<iostream>
using namespace std;

struct List 
{
	List *next;
	List *prev;
	List() : next(this), prev(this) {}
	List(List *nxt, List *prv) : next(nxt), prev(prv) 
	{
		nxt->prev = prv->next = this;
	}

	virtual ~List() 
	{
		next->prev = prev;
		prev->next = next;
	}
};
struct Node;

class Print
{
public:
	Print(const char* title, const char * author, int pages = 0, int year = 0)
	{
		strcpy(Title, title);
		strcpy(Author, author);
		Pages = pages;
		Year = year;
		cout << endl << "Using List constructor" << endl;
	}

	static List list;
	static void print_all();
	virtual void Show() {};
	Print(const Print&) {}
	virtual ~Print();
	char* GetTitle()
	{
		return Title;
	}
	char* GetAuthor()
	{
		return Author;
	}
	int GetPages()
	{
		return Pages;
	}
	int GetYear()
	{
		return Year;
	}
protected:

	char Title[50];
	char Author[50];
	int Pages;
	int Year;
};

List Print::list;

struct Point : public List 
{
	Point() : List() {}
	Point(List *nxt, List *prv, Print *person_)
		: List(nxt, prv), person(person_) {}
	Print *person;
};

void Print::print_all() 
{
	List *current = list.next;
	while (current != &list) 
	{
		(static_cast<Point*>(current))->person->print_all();
		current = current->next;
	}
}

Print::~Print() {
	List *point = list.next;
	while (static_cast<Point*>(point)->person != this && point != &list)
		point = point->next;
	if (point != &list)
		delete point;
}

class Book : virtual public Print
{
public:
	Book(const char* title, const char * author, const char * publisher, int pages = 0, int year = 0) : Print(title, author, pages, year)
	{
		strcpy(Publisher, publisher);
		cout << "Using Book constructor" << endl;
	}
	void Show()
	{
		cout << endl << "Type: Book" << endl;
		cout << "Title: " << Title << endl;
		cout << "Author(s): " << Author << endl;
		cout << "Publisher: " << Publisher << endl;
		cout << "Pages: " << Pages << endl;
		cout << "Year: " << Year << endl;
	}
	char* GetPublisher()
	{
		return Publisher;
	}
protected:
	char Publisher[25];
};

class Magazine : public Print
{
public:
	Magazine(const char* title, const char * author, int issueNumber, int pages = 0, int year = 0) : Print(title, author, pages, year)
	{
		IssueNumber = issueNumber;
		cout << "Using Magazine constructor" << endl;
	}
	int GetIssueNumber()
	{
		return IssueNumber;
	}
	void Show()
	{
		cout << endl << "Type: Magazine" << endl;
		cout << "Title: " << Title << endl;
		cout << "Author: " << Author << endl;
		cout << "Issue Number: " << IssueNumber << endl;
		cout << "Pages: " << Pages << endl;
		cout << "Year: " << Year << endl;
	}
protected:
	int IssueNumber;
};

class TextBook : public Print
{
public:
	TextBook(const char* title, const char * author, const char * publisher, int part, int pages = 0, int year = 0) : Print(title, author, pages, year)
	{
		strcpy(Publisher, publisher);
		Part = part;
		cout << "Using TextBook constructor" << endl;
	}
	char* GetPublisher()
	{
		return Publisher;
	}
	int GetPart()
	{
		return Part;
	}
	void Show()
	{
		cout << endl << "Type: TextBook" << endl;
		cout << "Title: " << Title << endl;
		cout << "Author(s): " << Author << endl;
		cout << "Publisher: " << Publisher << endl;
		cout << "Part: " << Part << endl;
		cout << "Pages: " << Pages << endl;
		cout << "Year: " << Year << endl;
	}
protected:
	int Part;
	char Publisher[25];
};

int main()
{
	Book *a = new Book("Game of Thrones", "G. Martin", "ACT", 768, 2019);
	a->Show();
	Magazine *b = new Magazine("Mighty Car Mods", "M&M", 8, 52, 2018);
	b->Show();
	TextBook *c = new TextBook("ABC", "Goreckiy V., Kiryushkin V. and and others", "Education",1, 127, 2017);
	c->Show();
	system("pause");
	return 0;
}