Input your Array1(Enter a dot to complete the input): a b c d e .

Input your Array2(Enter a dot to complete the input): d e f.

Choose what you want to do: 
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 1
Arr1: a b c d e

Arr2: d e f

If you want continue, press: 1; else press any button
Your choice - 1

Choose what you want to do: 
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 2

Affiliation check 'a' to array1: True
If you want continue, press: 1; else press any button
Your choice - 1

Choose what you want to do: 
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 3

Arr2 is subset arr1: False

If you want continue, press: 1; else press any button.
Your choice - 1

Choose what you want to do:
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 4

Intersection of arr2 and arr1: d e

If you want continue, press: 1; else press any button.
Your choice - 1

Choose what you want to do:
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 5
Invalid item, try again.

Choose what you want to do:
1 - Show your arrays.
2 - Affiliation check 'a' to array1.
3 - Checking array 2 for a subset of array 1.
4 - Checking intersection of array2 and array1
0  - Exit
Your choice - 0
Thanks for the knowledge! Goodbye.