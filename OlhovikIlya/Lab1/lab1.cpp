#include <iostream>
#include <math.h>
using namespace std;
int main()
{
   float a=1000, b=0.0001, zad11;
   double a1=1000, b1=0.0001, zad12;
   int zad21,zad22,zad23,m1=0,n1=0,m2=0,n2=0,m3=0,n3=0;
   zad11=(pow(a-b,3)-(pow(a,3)-3*a*pow(b,2)))/(pow(b,3)-3*pow(a,2)*b);
   zad12=(pow(a1-b1,3)-(pow(a1,3)-3*a1*pow(b1,2)))/(pow(b1,3)-3*pow(a1,2)*b1);
   cout<< "The value of the expression when the data type float: " << zad11 <<endl;
   cout<< "The value of the expression when the data type double: " << zad12 <<endl;
   cout<<"The starting values of the variables: "<< "m="<<m1 << "  n="<<n1<<endl;
   zad21=m1-++n1;   //0-1 - -1 
   zad22=++m2>--n2; //1>-1 - 1 - true
   zad23=--n3<++m3; //-1<1 - 1 - true
   cout<<"The value of the first expression: "<<zad21<<endl;
   cout<<"The value of the second expression: "<<zad22<<endl;
   cout<<"The value of the third expression: "<<zad23<<endl;
   return 0;
}
