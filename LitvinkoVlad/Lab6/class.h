#ifndef CLASS_H
#include <iostream>
using namespace std;
class kadr
{
protected:
	static kadr* HEAD;
	kadr* next;
	char *name;
	int date;
public:
	static void Print();
	kadr(const char *name, int date);
	kadr();
	kadr(kadr&copy);
	~kadr();
	virtual void Show() = 0;
	void Add();
	void SetName(const char*name);
	void SetDate(int date);
};
class admin : public kadr
{
private:
	int ludi;
public:
	admin(const char *name, int date, int ludi);
	admin();
	admin(admin&copy);
	~admin();
	void Show();
	void SetLudi(int ludi);
};
class rabochiy : public kadr
{
protected:
	int zarplata;
public:
	rabochiy(const char *name, int date, int zarplata);
	rabochiy();
	rabochiy(rabochiy &copy);
	~rabochiy();
	void Show();
	void SetZarplata(int zarplata);
};
class inzhener : public rabochiy
{
private:
	int razryad;
public:
	inzhener(const char *name, int date, int zarplata, int razryad);
	inzhener();
	inzhener(inzhener&copy);
	~inzhener();
	void Show();
	void SetRazryad(int razryad);
};
#endif CLASS_H
