#include "Class.h"
kadr* kadr::HEAD = NULL;
void kadr::Print()
{
	if (HEAD != NULL)
		for (kadr*p = HEAD; p != NULL; p = p->next)
			p->Show();
	else
		cout << "������ �������� ����." << endl;
}
kadr::kadr(const char *name, int date)
{
	this->name = new char[strlen(name) + 1];
	strcpy_s(this->name, strlen(name) + 1, name);
	this->date = date;
	if (HEAD == NULL)
	{
		HEAD = this;
		HEAD->next = NULL;
	}
	else
	{
		kadr *p;
		for (p = HEAD; p->next != NULL; p = p->next);
		p->next = this;
		p->next->next = NULL;
	}
	cout << "����������� ������ ����� ��������" << endl;
}
kadr::kadr()
{
	name = NULL;
	this->date = 0;
	cout << "����������� �� ��������� ������ ����� ��������" << endl;
}
kadr::kadr(kadr&copy)
{
	if (copy.name != NULL)
	{
		name = new char[strlen(copy.name) + 1];
		strcpy_s(name, strlen(copy.name) + 1, copy.name);
	}
	else
		name = NULL;
	kadr *p = HEAD;
	for (; p->next != NULL; p = p->next);
	p->next = this;
	p->next->next = NULL;
	date = copy.date;
	cout << "����������� ����������� ������ ����� ��������" << endl;
}
kadr::~kadr()
{
	if (name != NULL)
		delete[] name;
	if (HEAD->next == NULL)
		HEAD = NULL;
	else
	{
		kadr *p;
		for (p = HEAD; p->next->next != NULL; p = p->next);
		p->next = NULL;
	}
	cout << "���������� ������ ����� ��������" << endl;
};
void kadr::Add()
{
	if (HEAD == NULL)
	{
		HEAD = this;
		HEAD->next = NULL;
	}
	else
	{
		kadr *p;
		for (p = HEAD; p->next != NULL; p = p->next);
		p->next = this;
		p->next->next = NULL;
	}
}
void kadr::SetName(const char*name)
{
	if (this->name != NULL)
		delete[] this->name;
	this->name = new char[strlen(name) + 1];
	strcpy_s(this->name, strlen(name) + 1, name);
}
void kadr::SetDate(int date)
{
	this->date = date;
}
admin::admin(const char *name, int date, int ludi) : kadr(name, date)
{
	this->ludi = ludi;
	cout << "����������� ������ ������������� ��������" << endl;
}
admin::admin() : kadr()
{
	this->ludi = ludi;
	cout << "����������� �� ��������� ������ ������������� ��������" << endl;
}
admin::admin(admin&copy) : kadr(copy)
{
	ludi = copy.ludi;
	cout << "����������� ����������� ������ ������������� ��������" << endl;
}
admin::~admin() { cout << "���������� ������ ������������� ��������" << endl; }
void admin::Show()
{
	if (name == NULL)
		cout << "���: �� ����������" << endl;
	else
		cout << "���: " << name << endl;
	cout << "���: �������������" << endl;
	cout << "���� ������ ������:" << date << endl;
	cout << "����� ����� � ����������:" << ludi << endl << endl;
}
void admin::SetLudi(int ludi)
{
	this->ludi = ludi;
}
rabochiy::rabochiy(const char *name, int date, int zarplata) : kadr(name, date)
{
	this->zarplata = zarplata;
	cout << "����������� ������ ������� ��������" << endl;
}
rabochiy::rabochiy() : kadr()
{
	this->zarplata = 0;
	cout << "����������� �� ��������� ������ ������� ��������" << endl;
}
rabochiy::rabochiy(rabochiy&copy) : kadr(copy)
{
	zarplata = copy.zarplata;
	cout << "����������� ����������� ������ ������� ��������" << endl;
}
rabochiy::~rabochiy()
{
	cout << "���������� ������ ������� ��������" << endl;
}
void rabochiy::Show()
{
	if (name == NULL)
		cout << "���: �� ����������" << endl;
	else
		cout << "���: " << name << endl;
	cout << "���: �������" << endl;
	cout << "���� ������ ������:" << date << endl;
	cout << "��������: " << zarplata << "$" << endl << endl;
}
void rabochiy::SetZarplata(int zarplata)
{
	this->zarplata = zarplata;
}
inzhener::inzhener(const char *name, int date, int zarplata, int razryad) : rabochiy(name, date, zarplata)
{
	this->razryad = razryad;
	cout << "����������� ������ ������� ��������" << endl;
}
inzhener::inzhener() : rabochiy()
{
	this->razryad = 0;
	cout << "����������� �� ��������� ������ ������� ��������" << endl;
}
inzhener::inzhener(inzhener&copy) : rabochiy(copy)
{
	razryad = copy.razryad;
	cout << "����������� ����������� ������ ������� ��������" << endl;
}
inzhener::~inzhener() { cout << "���������� ������ ������� ��������" << endl; };
void inzhener::Show()
{
	if (name == NULL)
		cout << "���: �� ����������" << endl;
	else
		cout << "���: " << name << endl;
	cout << "���: �������" << endl;
	cout << "���� ������ ������:" << date << endl;
	cout << "��������:" << zarplata << "$" << endl;
	cout << "������: " << razryad << endl << endl;
}
void inzhener::SetRazryad(int razryad)
{
	this->razryad = razryad;
}