#include <iostream>
#include <fstream>
using namespace std;

int skobka_kryg_1 = 0, skobka_kryg_2 = 0;
int skobka_kvadr_1 = 0, skobka_kvadr_2 = 0;
int skobka_figur_1 = 0, skobka_figur_2 = 0;
int kavichka1 = 0;
int kavichka2 = 0;
int comment1 = 0, comment2 = 0;
void proverka();
char c;

int main()
{
	setlocale(LC_ALL, "rus");
	ifstream in;
	char adress[100];
	cout << "Entered adres file" << endl;
	cin >> adress;
	in.open(adress);
	if (!in.is_open())
	{
		cout << "File not open." << endl;
		system("pause");
		return -1;
	}
	bool isComment = false;

	while (!in.eof())
	{

		in.get(c);
		if (!in.eof())
		{

			if (c == '/')
			{

				in.get(c);
				if (c == '*')
				{

					if (!isComment)
						comment1++;
					isComment = true;

				}
				else if (c == '/')
				{

					for (bool key = true; key;)
					{
						in.get(c);
						if (c == '\n')
							key = false;
					}

				}
				else if (!isComment)
					proverka();
			}
			else if (c == '*')
			{

				in.get(c);
				if (c == '/')
				{

					comment2++;
					isComment = false;

				}
				else if (!isComment)
					proverka();

			}
			else  if (!isComment)
				proverka();
		}
	}
	bool error = false;
	if (skobka_kryg_1 < skobka_kryg_2)
	{
		cout << "Error: code not have " << abs(skobka_kryg_2 - skobka_kryg_1) << " символов (" << endl;
		error = true;
	}
	if (skobka_kryg_1 > skobka_kryg_2)
	{
		cout << "Error: code not have " << abs(skobka_kryg_2 - skobka_kryg_1) << " ñèìâîëîâ )" << endl;
		error = true;
	}
	if (skobka_kvadr_1 < skobka_kvadr_2)
	{
		cout << "Error: code not have " << abs(skobka_kvadr_2 - skobka_kvadr_1) << " ñèìâîëîâ [" << endl;
		error = true;
	}
	if (skobka_kvadr_1 > skobka_kvadr_2)
	{
		cout << "Error: code not have " << abs(skobka_kvadr_2 - skobka_kvadr_1) << " ñèìâîëîâ ]" << endl;
		error = true;
	}
	if (skobka_figur_1 < skobka_figur_2)
	{
		cout << "Error: code not have " << abs(skobka_figur_2 - skobka_figur_1) << " ñèìâîëîâ {" << endl;
		error = true;
	}
	if (skobka_figur_1 > skobka_figur_2)
	{
		cout << "Error: code not have " << abs(skobka_figur_2 - skobka_figur_1) << " ñèìâîëîâ }" << endl;
		error = true;
	}
	if (kavichka1 % 2 != 0)
	{
		cout << "Error: in code not opened \'" << endl;
		error = true;
	}
	if (kavichka2 % 2 != 0)
	{
		cout << "Error: in code not opened \"" << endl;
		error = true;
	}
	if (comment1<comment2)
	{
		cout << "Error: in code have superfluous symbols " << abs(comment2 - comment1) << " ñèìâîëû êîíöà êîììåíòàğèÿ */" << endl;
		error = true;
	}
	if (comment1 > comment2)
	{
		cout << "Error: in code has not been closed /*" << endl;
		error = true;
	}
	if (!error)
		cout << "There are no syntax errors in the code." << endl;
	in.close();
	system("pause");
	return 0;
}
void proverka()
{
	if (c == '(')
		skobka_kryg_1++;
	else if (c == ')')
		skobka_kryg_2++;
	else if (c == '[')
		skobka_kvadr_1++;
	else if (c == ']')
		skobka_kvadr_2++;
	else if (c == '{')
		skobka_figur_1++;
	else if (c == '}')
		skobka_figur_2++;
	else if (c == '\'')
		kavichka1++;
	else if (c == '\"')
		kavichka2++;
}