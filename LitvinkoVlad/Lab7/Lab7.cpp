#include <iostream>
#include <windows.h>
using namespace std;
const int max = 25;
class Set
{
	int length;
	Set *HEAD, *next;
	char c;
	void concat(char);
public:
	Set(char);
	Set(Set&);
	~Set();
	void add(char);
	void input();
	void print();
	Set& operator = (Set&);
	void operator - (char);
	bool operator != (Set&);
	bool operator > (Set&);
};
Set::Set(char c) {
	length = 1;
	HEAD = this;
	this->c = c;
	next = NULL;
}
Set::Set(Set &other) {
	HEAD = this;
	length = 1;
	c = other.HEAD->c;
	Set *p = HEAD, *t = other.HEAD;
	t = t->next;
	for (; t != NULL; t = t->next)
	{
		p->next = new Set(t->c);
		p = p->next;
		length++;
	}
}
Set::~Set() {
	if (next != NULL) {
		delete next;
	}
}
void Set::add(char c) {
	if (length < max) {
		concat(c);
	}
}
void Set::input() {
	if (length < max) {
		char c;
		cout << "Enter a character in the set:";
		cin >> c;
		concat(c);
	}
}
void Set::concat(char c) {
	if (c < HEAD->c)
	{
		Set *p = HEAD;
		HEAD = new Set(c);
		HEAD->next = p;
	}
	else
	{
		if (HEAD->next == NULL)
		{
			HEAD->next = new Set(c);
		}
		else
		{
			Set *p = HEAD;
			for (; p->next != NULL && p->next->c < c; p = p->next);
			if (p->next == NULL)
				p->next = new Set(c);
			else if (p->next->c > c)
			{
				Set *t = p->next;
				p->next = new Set(c);
				p->next->next = t;
			}
		}
	}
	length++;
}
void Set::print() {
	for (Set *p = HEAD; p != NULL; p = p->next)
		cout << p->c << "|";
	cout << endl;
}
Set& Set::operator = (Set& other) {
	if (next != NULL)
		delete next;
	length = 1;
	c = other.HEAD->c;
	Set *p = HEAD, *t = other.HEAD;
	t = t->next;
	for (; t != NULL; t = t->next)
	{
		p->next = new Set(t->c);
		p = p->next;
		length++;
	}
	return *this;
}
bool Set::operator != (Set& other) {
	if (length != other.length)
	{
		return true;
	}
	else {
		for (Set *p = HEAD, *t = other.HEAD; p != NULL; p = p->next, t = t->next)
			if (p->c != t->c)
				return true;
		return false;
	}
}
void Set::operator - (char c)
{
	Set *p = HEAD;
	for (; p != NULL && p->c != c; p = p->next);
	if (p != NULL)
	{
		if (p == HEAD && p->next != NULL)
		{
			HEAD = HEAD->next;
			p->next = NULL;
			delete p;
		}
		else
		{
			Set *t = HEAD;
			for (; t->next != p; t = t->next);
			if (p->next == NULL)
				t->next = NULL;
			else
				t->next = p->next;
			p->next = NULL;
			delete p;
		}
	}
}
bool Set::operator > (Set &other)
{
	if (other.length > length)
		return false;
	else
	{
		Set *p;
		for (Set *t = other.HEAD; t != NULL; t = t->next)
		{
			for (p = HEAD; p != NULL && p->c != t->c; p = p->next);
			if (p == NULL)
				return false;
		}
		return true;
	}
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	////////////////
	Set set1('f');
	set1.add('a');
	set1.add('h');
	set1.add('y');
	set1.add('b');
	////////////////
	Set set2('d');
	set2.add('e');
	set2 = set1;
	set2.input();
	////////////////
	Set set3 = set1;
	////////////////
	Set set4('a');
	set4.add('f');
	set4.add('h');
	///////////////
	set2 - 'h';
	////////////////
	cout << "set1:";
	set1.print();
	cout << "set2:";
	set2.print();
	cout << "set3:";
	set3.print();
	cout << "set4:";
	set4.print();
	////////////////
	if (set1 != set2)
		cout << "set1!=set2" << endl;
	else
		cout << "set1=set2" << endl;
	////////////////////////////////////
	if (set1 != set3)
		cout << "set1!=set3" << endl;
	else
		cout << "set1=set3" << endl;
	/////////////////////////////////
	if (set1 > set4)
		cout << "set4 is a subset set1" << endl;
	else
		cout << "set4 not a subset set1" << endl;
	if (set2 > set4)
		cout << "set4 is a subset set2" << endl;
	else
		cout << "set4 not a subset set2" << endl;
	if (set3 > set4)
		cout << "set4 is a subset set3" << endl;
	else
		cout << "set4 not a subset set3" << endl;
	system("pause");
	return 0;
}