Zad 1


The number of items you want to enter:
5
Insert the number 1:
5
Insert the number 2:
-7
Insert the number 3:
8
Insert the number 4:
4
Insert the number 5:
-6
New sequence (only non-negative values):
5|8|4|
Zad 2


The number of items you want to enter:
4
Insert the number 1:
5
Insert the number 2:
0
Insert the number 3:
-7
Insert the number 4:
3
Only one zero was found in the sequence.