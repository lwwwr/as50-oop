#include <QCoreApplication>
#include <QTextStream>
#include <QTextCodec>
#include <QVector>
int main(int argc, char *argv[])
{
   QCoreApplication a(argc, argv);
   QTextStream in(stdin);
   QTextStream out(stdout);
   out.setCodec(QTextCodec::codecForName("cp866"));
   out << QString("Zad 1") << endl << endl << endl;
   QVector<double> vector1;
   int length;
   out << QString("The number of items you want to enter:") << endl;
   in >> length;
   double input;
   for(int i=0; i<length; i++)
   {
          out << QString("Insert the number ") << i+1 << ":" << endl;
          in >> input;
          vector1.append(input);
   }
   QVector<double> vector2;
   for(int i=0; i<vector1.length(); i++)
         if(vector1[i]>=0)
         vector2.append(vector1[i]);
         out << QString("New sequence (only non-negative values):") << endl;
   for(int i=0; i<vector2.length(); i++)
         out << vector2[i] << "|";
   out << endl;
   out << endl << endl << endl << QString("Zad 2") << endl << endl << endl;
   QVector<double> vector3;
   out << QString("The number of items you want to enter:") << endl;
   in >> length;
   for(int i=0; i<length; i++)
   {
         out << QString("Insert the number ") << i+1 << ":" << endl;
         in >> input;
         vector3.append(input);
   }
   QVector<double>::iterator itstart, itend;
   for(itstart=vector3.begin(); (itstart!=vector3.end())&&(*itstart!=0); itstart++);
   for(itend=vector3.end(); (itend!=vector3.begin())&&(*itend!=0); itend--);
   if(itstart==vector3.end())
   out << QString("Initial zero element not found.") << endl;
   else if(itstart==itend)
   out << QString("Only one zero was found in the sequence.") << endl;
   else//если 2 и более нуля
   {
       double sum=0;
       for(itstart++; itstart!=itend; itstart++)
       sum+=*itstart;
       out << QString("The sum of the numbers between the first and last zero value:") << endl;
       out << sum << endl;
   }
   return 0;
}
