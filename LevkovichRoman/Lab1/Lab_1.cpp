#include <iostream>
#include <math.h>
using namespace std;
int main()
{
	int n, m;
	float a = 1000, b = 0.0001,e=500,g=0.0002,zad1,dop_zad1;
	double c = 1000, d = 0.0001,t=500,u=0.0002, zad2, dop_zad2;
	zad1 = (pow(a + b, 2) - (pow(a, 2) + 2 * a*b)) / (pow(b, 2));
	cout << "Result of zad1:  " << zad1 << endl;
	dop_zad1 = (pow(e + g, 2) - (pow(e, 2) + 2 * e*g)) / (pow(g, 2));
	cout << "Result of dop_zad1:  " << zad1 << endl;
	zad2 = (pow(c + d, 2) - (pow(c, 2) + 2 * c*d)) / (pow(d, 2));
	cout << "Result of zad2:  " << zad1 << endl;
	dop_zad2 = (pow(t + u, 2) - (pow(t, 2) + 2 * t*u)) / (pow(u, 2));
	cout << "Result of dop_zad2:  " << zad1 << endl;
	cout << "Enter equal n" << endl;
	cin >> n;
	cout << "Enter equal m" << endl;
	cin >> m;
	cout << "Result of zad3((++n)*(++m)):   " << (++n)*(++m) << endl;
	if ((m++) < n) {
		cout << "Result of ((m++) < n):  " << ((m++) < n) << endl;
	}
	else {
		cout << "Expression (m++ > n) does not satisfy the condition:  " << endl;
	}
	if ((n++) > m) {
		cout << "Result of ((n++) > m):  " << ((n++) > m) << endl;
	}
	else {
		cout << "Expression ((n++) > m) does not satisfy the condition:  " << endl;
	}
	system("pause");
	return 0;
}