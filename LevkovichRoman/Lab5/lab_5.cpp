#include <iostream>
#include <QVector>
using namespace std;
void zadanie1(){
    cout << "Condition 1"<<endl;
    QVector<int> vector = {0,0,0,-4,4,1,1,-6,-7,8};
    QVector<int> rezult;
    int number;
    foreach (number, vector)
        if(number == 0) rezult += number;
    foreach (number, vector)
        if(number > 0) rezult += number;
    foreach (number, vector)
        if(number < 0) rezult += number;
    foreach (number, vector)
        cout << number << " ";
    cout <<endl<< "result "<<endl;
    foreach (number, rezult)
        cout << number << " ";
}
void zadanie2() {
    cout << endl<< "Condition 2"<<endl;
    QVector<int> vector = {1,5,3,2,0,0,-4,2,-6,-1,-5};
    QVectorIterator<int> I(vector);
    int summ = 0;
    bool ok = false;
    while(!ok && I.hasNext()){
        if(I.next() < 0 && I.hasNext()){
            while(I.peekNext() >= 0 && I.hasNext())
                summ += I.next();
        ok = true;
        }
    }
    foreach (int number, vector) cout << number << " ";
    cout << endl<< "sum " <<endl<< summ  ;
}
int main(){
    zadanie1();
    zadanie2();
}
