﻿#include <QtCore/QCoreApplication>
#include <QCoreApplication>
#include <QVector>
#include <QtDebug>
#include<iostream>
using namespace std;

int main(int argc, char* argv[]) 
{
	QCoreApplication a(argc, argv);
	double b;
	// объявили 2 контейнера
	QVector <double> in, out;
	cout << "Enter 9 elements of sequence(first exercise):" << endl;
	for (int i = 0; i < 9; i++) {
		cin >> b;
		in << b;
	}
	//объявили итератор
	QVector<double>::const_iterator it;
	//выведем на экран
	qDebug() << "Input :" << in;
	//цикл для прохода с начала в конец и проверяем, если 0 то в начало выходного контейнера, иначе то в конец
	for (it = in.constBegin(); it != in.constEnd(); ++it) {
		if (*it == .0) {
			out.push_front(*it);
			continue;
		}
		out.push_back(*it);
	}
	//выводим результат
	qDebug() << "Output :" << out;

	//выполним используя другой контейнер - QList
	QList <double> list;
	cout << "Enter 9 elements of sequence(second exercise):" << endl;
	for (int i = 0; i < 9; i++) {
		cin >> b;
		list << b;
	}
	qDebug() << "Input: " << list;
	//итератор для чтения-записи
	QMutableListIterator<double>  lit(list);

	double sum;
	// удаляем с начала контейнера не положительные числа
	while (lit.hasNext()) {
		if (lit.next() <= 0) {
			lit.remove();
			continue;
		}
		break;
	}
	//переместим итератор в конец
	lit.toBack();
	while (lit.hasPrevious()) {
		if (lit.previous() <= 0) {
			lit.remove();
			continue;
		}
		break;
	}
	//итератор в начале
	lit.toFront();
	//считаем все что в между
	while (lit.hasNext()) {
		sum += lit.next();
	}
	qDebug() << "Summa is " << sum;
	return a.exec();
}