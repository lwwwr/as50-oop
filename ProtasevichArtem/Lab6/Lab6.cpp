#include<stdio.h>
#include<iostream>
using namespace std;

struct List
{
	List* next;
	List* prev;
	List() : next(this), prev(this) {}
	List(List* nxt, List* prv) : next(nxt), prev(prv)
	{
		nxt->prev = prv->next = this;
	}

	virtual ~List()
	{
		next->prev = prev;
		prev->next = next;
	}
};
struct Node;

class Checkout
{
public:
	Checkout(const char* name,  int number = 0)
	{
		strcpy_s(Name, name);
		Number = number;
		cout << endl << "Using Checkout constructor" << endl;
	}

	static List list;
	static void print_all();
	virtual void Show() {};
	Checkout(const Checkout&) {}
	virtual ~Checkout();
	char* GetName()
	{
		return Name;
	}
	int GetNumber()
	{
		return Number;
	}
protected:
	char Name[50];
	int Number;
};

List Checkout::list;

struct Point : public List
{
	Point() : List() {}
	Point(List* nxt, List* prv, Checkout* person_)
		: List(nxt, prv), person(person_) {}
	Checkout* person;
};

void Checkout::print_all()
{
	List* current = list.next;
	while (current != &list)
	{
		(static_cast<Point*>(current))->person->print_all();
		current = current->next;
	}
}

Checkout::~Checkout() {
	List* point = list.next;
	while (static_cast<Point*>(point)->person != this && point != &list)
		point = point->next;
	if (point != &list)
		delete point;
}

class Test : virtual public Checkout
{
public:
	Test(const char* name,const char* theme, int number = 0) : Checkout(name,number)
	{
		strcpy_s(Theme, theme);
		cout << "Using Test constructor" << endl;
	}
	void Show()
	{
		cout << endl << "Type: Test" << endl;
		cout << "Name: " << Name << endl;
		cout << "Theme of test: " << Theme << endl;
		cout << "Number of test: " << Number << endl;
	}
	char* GetTheme()
	{
		return Theme;
	}
protected:
	char Theme[25];
};

class Exam : public Checkout
{
public:
	Exam(const char* name, const char* theme, const char* teacher, int number = 0) : Checkout(name, number)
	{
		strcpy_s(Teacher, teacher);
		strcpy_s(Theme, theme);
		cout << "Using Exam constructor" << endl;
	}
	char* GetTeacher()
	{
		return Teacher;
	}
	char* GetTheme()
	{
		return Theme;
	}
	void Show()
	{
		cout << endl << "Type: Exam" << endl;
		cout << "Name of exam: " << Name << endl;
		cout << "Theme: " << Theme << endl;
		cout << "Teacher: " << Teacher << endl;
		cout << "Number of question: " << Number << endl;
		
	}
protected:
	char Teacher[25];
	char Theme[25];
};

class FinalExam : public Checkout
{
public:
	FinalExam(const char* name, const char* theme, const char* teacher, int number=0) : Checkout(name,number)
	{
		strcpy_s(Teacher,teacher);
		strcpy_s(Theme, theme);
		cout << "Using FinalExam constructor" << endl;
	}
	char* GetTeacher()
	{
		return Teacher;
	}
	char* GetTheme()
	{
		return Theme;
	}
	void Show()
	{
		cout << endl << "Type: FinalExam" << endl;
		cout << "Name: " << Name << endl;
		cout << "Theme: " << Theme << endl;
		cout << "Teacher: " << Teacher << endl;
		cout << "Number of question: " << Number << endl;
	}
protected:
	char Teacher[25];
	char Theme[25];
};

int main()
{
	Test* a = new Test("Math", "Probability theory", 5);
	a->Show();
	Exam* b = new Exam("MMIPiU", "OLL","Chercchel", 8);
	b->Show();
	FinalExam* c = new FinalExam("OOP", "OLL", "Dunec", 6);
	c->Show();
	system("pause");
	return 0;
}