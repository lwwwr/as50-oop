﻿// SEM4 2.2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <cctype>
#include <algorithm>
using namespace std;

int main()
{
	char str[256], n[256], v[256];
	int i, s1=0,s2=0;
	cout << "Enter a string:"<<endl;
	cin.getline(str, 256);
	cout << endl;
	do {
		for (i = 0; i < strlen(str); i++) {
			if (!isspace(str[i])) {
				if (islower(str[i])) {
					n[s1] = str[i];
					s1++;
				}
				else {
					v[s2] = str[i];
					s2++;
				}
			}
		}
	} while (i != strlen(str));
	cout << "Sorted string:"<<endl;
	sort(n,n+s1);
	sort(v,v+s2);
	for (int i = 0; i < s1; i++)
		cout << n[i];
	for (int i = 0; i < s2; i++)
		cout << v[i];
}
