﻿// SEM2 1.0.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <Windows.h> 
#include <iostream>
using namespace std;

void zadanie_1()
{
	float a, b, SF1, SF2, f1, f2, f3, f4, f5, f6, Sfloat;
	double c, e, SD1, SD2, d1, d2, d3, d4, d5, d6, Sdouble;
	a = 1000;
	b = 0.001;
	c = 1000;
	e = 0.001;
	f1 = pow(a - b, 3); f2 = pow(a, 3); f3 = 3 * a*pow(b, 2);
	f4 = pow(b, 3); f5 = 3 * a*pow(b, 2); f6 = 3 * pow(a, 2)*b;
	SF1 = f1 - (f2 - f3); SF2 = f4 - f5 - f6;
	Sfloat = SF1 / SF2;
	cout << "In float:" << endl << Sfloat << endl;
	d1 = pow(c - e, 3); d2 = pow(c, 3); d3 = 3 * c*pow(e, 2);
	d4 = pow(e, 3); d5 = 3 * c*pow(e, 2); d6 = 3 * pow(c, 2)*e;
	SD1 = d1 - (d2 - d3);
	SD2 = d4 - d5 - d6;
	Sdouble = SD1 / SD2;
	cout << "In double:" << endl << Sdouble<<endl;
	system("pause");
}
void zadanie_2()
{
	int n, m;
	cout << "Enter first number: ";
	cin >> n;
	cout << "Enter second number: ";
	cin >> m;
	cout << "1) " << m + --n << endl;
	cout << "2) " << "m++ > n++ ";
	if (m++ > n++) {
		cout << "true" << endl;
	}
	else cout << "false" << endl;
	cout << "3) " << "--n < ++m ";
	if (--n < ++m) {
		cout << "true" << endl;
	}
	else cout << "false" << endl;
	system("pause");
}
int main()
{
	setlocale(LC_ALL,"rus");
	int a = 1;
	do
	{
		system("cls");
		cout << " Выберите действие: " << endl
			<< " 1. Задание 1" << endl
			<< " 2. Задание 2" << endl
			<< " 0. Выход" << endl
			<< " Действие: ";
		cin >> a;
		switch (a)
		{
		case 1: zadanie_1(); break;
		case 2: zadanie_2(); break;
		case 0: return 1; break;
		default:cout << " Неверное значение"<<endl; system("pause"); break;
		}
	} while (1);
}