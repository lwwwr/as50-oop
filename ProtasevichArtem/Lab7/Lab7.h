#include <iostream>

struct count {
	count();
	count(char);
	count(const count*);
	count*apNext;
	char aElem;
};

count::count() : apNext(nullptr) {}
count::count(char elem) : apNext(nullptr), aElem(elem) {}
count::count(const count*obj) : apNext(nullptr), aElem(obj->aElem) {}

class List {
public:
	void PushEnd(char);
	char PopBeg();
	List& operator +(const List&);
	List& operator --();
	bool operator ==(const List&);
	void Input();
	void Print();
	List();
	List(char);
	List(const List&);
	void Add(char);
	~List();
private:
	int size;
	count* apBeg;
};

List::List(char elem) : List() {
	Add(elem);
}

List::List(const List &obj) : List() {
	count*pTmp = obj.apBeg;
	while (pTmp) {
		Add(pTmp->aElem);
		pTmp = pTmp->apNext;
	}
}

List::List() : size(0), apBeg(nullptr) {}

List& List::operator --() {
	PopBeg();
	return *this;
}

List& List::operator+(const List &obj) {
	List *pNewList = new List(*this);
	count*pCur = obj.apBeg;
	while (pCur) {
		pNewList->Add(pCur->aElem);
		pCur = pCur->apNext;
	}
	return *pNewList;
}

bool List::operator ==(const List &obj) {
	return (size == obj.size);
}

void List::Input() {
	char ch;
	while (std::cin >> ch) {
		if (ch == '.') return;
		Add(ch);
	}
}

void List::Print() {
	count*pCur = apBeg;
	while (pCur) {
		std::cout << pCur->aElem << " ";
		pCur = pCur->apNext;
	}
	std::cout << std::endl;
}

void List::Add(char elem) {
	if (!apBeg) {
		count*pNewNode = new count(elem);
		apBeg = pNewNode;
	}
	else PushEnd(elem);
}

void List::PushEnd(char elem) {
	count*pNewNode = new count(elem);
	count*pCur = apBeg;
	while (pCur->apNext) {
		if (pCur->aElem == elem) {
			delete pNewNode;
			return;
		}
		pCur = pCur->apNext;
	}
	pCur->apNext = pNewNode;
	size++;
}

char List::PopBeg() {
	char elem = apBeg->aElem;
	count*pTemp = apBeg;
	apBeg = apBeg->apNext;
	delete pTemp;
	size--;
	return elem;
}

List::~List() {
	while (apBeg) PopBeg();
}
