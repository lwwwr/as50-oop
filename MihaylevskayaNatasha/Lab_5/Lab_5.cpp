#include <QVector>
#include <QDebug>
#include <time.h>
#include <math.h>
#include <iostream>
using namespace std;
int* FillVector(int *vector,int count);
void showVector(QVector<int> Vector);
QVector<int>evenId(int *vector, int count, QVector<int> vector2);
QVector<int>oddId(int *vector, int count, QVector<int> vector2);
void exer1(const int count, int *vector, QVector<int> vector_);
void exer2(const int count, int *vector);
int* maxValue(const int count, int *vector);
int minValue(const int count, int *vector, int max);
void composition(int *vector, int *id);
int main()
{
    const int count=10;
    QVector<int> valVector(count);
    int *vector = valVector.data();
    vector=FillVector(vector,count);
    cout<< "TASK 1"<<endl;
    exer1(count,vector,valVector);
    cout<< "TASK 2"<<endl;
    exer2(count, vector);
    system("pause");
}
int* FillVector(int *vector, int count) {
    srand(time(0));
    for (int i = 0; i < count; i++) {
        int val = rand() % 50-25;
        vector[i] = val;
    }
    return vector;
}
void showVector(QVector<int> Vector) {
    QVector<int>::const_iterator i;
    for (i = Vector.begin(); i != Vector.end(); ++i)
    {
        qDebug() << *i;
    }
}
QVector<int>evenId(int *vector, int count, QVector<int> vector2) {
    for (int i = 0; i < count; i++) {
        if (i % 2 == 0) vector2.push_back(vector[i]);
    }
    return vector2;
}
QVector<int>oddId(int *vector, int count, QVector<int> vector2) {
    for (int i = 0; i < count; i++) {
        if (i % 2 != 0) vector2.push_back(vector[i]);
    }
    return vector2;
}
void exer1(const int count, int *vector, QVector<int> vector_) {
    cout << " Value of vector" << endl;
    showVector(vector_);
    QVector<int> newVector;
    newVector = evenId(vector, count, newVector);
    newVector = oddId(vector, count, newVector);
    cout << "Result" << endl;
    showVector(newVector);
}
void exer2(const int count, int *vector) {
    int *id = new int[2];
    id = maxValue(count, vector);
    composition(vector, id);
}
int* maxValue(const int count, int *vector) {
    int *id = new int[2]; int max = 0;
    for (int i = 0; i < count; i++) {
        if (abs(vector[i]) > max) {
            max = abs(vector[i]);
            id[0] = i;
        }
    }
    id[1] = minValue(count,vector,max);
    return id;
}
int minValue(const int count, int *vector, int max) {
    int id; int min = max;
    for (int i = 0; i < count; i++) {
        if (abs(vector[i]) < max) {
            max = abs(vector[i]);
            id = i;
        }
    }
    return id;
}
void composition(int *vector, int *id) {
    int comp = 1;
    if(id[0] < id[1]) {
        for (int i = id[0] + 1; i < id[1]; i++) {
            comp *= vector[i];
        }
    }
    else {
        for (int i = id[1] + 1; i < id[0]; i++) {
            comp *= vector[i];
        }
    }
    cout << "Composition elements between max and min elements: " << comp << endl;
}
