﻿// lab1(2).cpp: определяет точку входа для консольного приложения.
//


#include "stdafx.h"
#include<math.h>
#include<iostream>
#include<stdio.h>
using namespace std;

int main()
{
	float a1, b1, c1;
	double a2, b2, c2;
	cout << "Enter a1:";
	cin >> a1;
	cout << "Enter b1:";
	cin >> b1;
	c1 = (pow(a1 + b1, 3) - pow(a1, 3)) / (3 * a1*pow(b1, 2)) + pow(b1, 3) + 3 * pow(a1, 2)*b1;
	cout << "Float:";
	printf("%.10f", c1);
	cout << "\n Enter a2:";
	cin >> a2;
	cout << "Enter b2:";
	cin >> b2;
	c2 = (pow(a2 + b2, 3) - pow(a2, 3)) / (3 * a2*pow(b2, 2)) + pow(b2, 3) + 3 * pow(a2, 2)*b2;
	cout << "\n Double:";
	printf("%.10f", c2);
	cout << "\n";
	system("pause");
	return 0;
}
