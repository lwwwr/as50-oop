#include <iostream>
using namespace std;
struct NodeBase {
	NodeBase *next;
	NodeBase *prev;
	NodeBase() : next(this), prev(this) {}
	NodeBase(NodeBase *next_, NodeBase *prev_) : next(next_), prev(prev_) {
		next_->prev = prev_->next = this;
	}
	virtual ~NodeBase() {
		next->prev = prev;
		prev->next = next;
	}
};
struct Node;
class detail{
protected:
	static int id;
	char material[50];
	char country[50];
public:
	detail(){
		id++;
		char *material_ = new char[50];
		char *country_ = new char[50];
		material_ = { "0" };
		country_ = { "0" };
		strcpy(material, material_);
		strcpy(country, country_);
		cout << "Using Base defalt constructor " << endl;
	}
	detail(char *material_, char *country_){
		id++;
		strcpy(material, material_);
		strcpy(country, country_);
		cout << "Using Base constructor " << endl;
	}
	void setMaterial(char *material_){
		strcpy(material, material_);
	}
	void setCountry(char *country_){
		strcpy(country, country_);
	}
	char* getMat(){
		return material;
	}
	char* getCountry(){
		return country;
	}
	static int getId(){
		return id;
	}
	virtual void Show() = 0;
	detail(const  detail&);
	static NodeBase base;
	static void print_all();
	virtual ~detail();
};
NodeBase  detail::base;
struct Node : public NodeBase {
	Node() : NodeBase() {}
	Node(NodeBase *next_, NodeBase *prev_, detail *person_)
		: NodeBase(next_, prev_), detail(person_) {}
	detail * detail;
};
void  detail::print_all() {
	NodeBase *current = base.next;
	while (current != &base) {
		(static_cast<Node*>(current))->detail->print_all();
		current = current->next;
	}
}
detail::~detail() {
	NodeBase *node = base.next;
	while (static_cast<Node*>(node)->detail != this && node != &base)
		node = node->next;
	if (node != &base)
		delete node;
}
class machine :virtual public  detail {
public:
	machine() : detail(){
		char *type_ = new char[50];
		type_ = { "0" };
		strcpy(type, type_);
		cout << "Using machine defalt constructor " << endl;
	};
	machine(char *material_, char *country_, char *type_) : detail(material_,country_){
		strcpy(type, type_);
		cout << "Using machine constructor " << endl;
	};
	void setType(char *type_){
		strcpy(type, type_);
	}
	char * getType(){
		return type;
	}
	void Show()
	{
		cout << " id: " << id << endl;
		cout << " material detail: " << material << endl;
		cout << " country of origin: " << country << endl;
		cout << " type machine: " << type << endl;
	}
protected:
	char type[50];
};
class product : virtual public detail {
public:
	product() :detail(){
		prise = 0;
		amount = 0;
		cout << "Using product defalt constructor " << endl;
	};
	product(char *material_, char *country_,int prise_=0,int amount_=0 ) :detail(material_,country_){
		prise = prise_;
		amount = amount_;
		cout << "Using product constructor " << endl;
	};
	void Show()
	{
		cout << " id: " << id << endl;
		cout << " material detail: " << material << endl;
		cout << " country of origin: " << country << endl;
		cout << " Prise product: " << prise << endl;
		cout << " Amount product: " << amount << endl;
	}
	int  getPrise(){
		return prise;
	}
	int getAmount(){
		return amount;
	}
	void setPrise(int prise_){
		prise = prise_;
	}
	void setAmount(int amount_){
		amount = amount_;
	}
protected:
	int prise;
	int amount;

};
class knot :virtual public detail {
public:
	knot() :detail(){
		char *type_ = new char[50];
		type_ = { "0" };
		char *function_ = new char[50];
		function_ = { "0" };
		strcpy(type, type_);
		strcpy(function, function_);
		cout << "Using Worker defalt constructor " << endl;
	};
	knot(char *material_, char *country_, char *type_, char *function_) :detail(material_,country_){
		strcpy(type, type_);
		strcpy(function, function_);
		cout << "Using Worker constructor " << endl;
	};
	void Show()
	{
		
			cout << " id: " << id << endl;
			cout << " material detail: " << material << endl;
			cout << " country of origin: " << country << endl;
			cout << " Type knot: " << type << endl;
			cout << " Function knot: " << function << endl;
		
	}
	void setType(char *type_){
		strcpy(type, type_);
	}
	char * getType(){
		return type;
	}
	void setFunction(char *function_){
		strcpy(function, function_);
	}
	char * getFunction(){
		return function;
	}
protected:
	char type[50];
	char function[50];
};
int detail::id = 0;
void main()
{
	machine *obj1 = new machine("steel", "USA","executive");
	obj1->Show();
	product *obj2 = new product("wood", "Belarus", 100,500);
	obj2->Show();
	knot *obj3 = new knot("steel", "Germany", "composite","load distribution");
	obj3->Show();
	system("pause");
}