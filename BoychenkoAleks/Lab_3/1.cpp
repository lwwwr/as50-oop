#include <QtCore/QCoreApplication>
#include <QString>
#include <QTextStream>
QTextStream cout(stdout);
QTextStream cin(stdin);

int main(int argc, char *argv[])
{	
	QString s1;
	QString s2;
	QString s3;
	QChar temp;	
	cout << "Enter sentence: "<< endl;
	s1 = cin.readLine();
	s1.replace(' ', "");
	int len = s1.length() / 2;
	s2 = s1.left(len);
	s3 = s1.right(len);
	for (int i = 0; i < s3.length() / 2; i++)
	{
		temp = s3[i];
		s3[i] = s3[s3.length() - 1 - i];
		s3[s3.length() - 1 - i] = temp;
	}
	if (!QString::compare(s2,s3, Qt::CaseInsensitive))
	{
		cout << "Palindrom" << endl;
	}
	else
	{
		cout << "Not palindrom" << endl;
	}
	system("pause>null");
}
