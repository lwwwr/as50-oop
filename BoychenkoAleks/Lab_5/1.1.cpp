#include <QtCore/QCoreApplication>
#include<QVector>
#include<iostream>
#include<ctime>
using namespace std;
int n = 10;
void show(QVector<int> &buf)
{
	for (int i = 0; i < buf.size(); i++)
		cout << i + 1 << ". " << buf[i] << endl;
}
void initialization(QVector<int> &buf)
{
	cout << "Start vector:" << endl;
	for (int i = 0; i < n; i++)
		buf[i] = rand() % 10 - 5;
	show(buf);
}
void without_zero(QVector<int> buf)
{
	QVector<int> zero;
	cout << "Vector without zero:" << endl;
	int j = 1;
	for (int i = 0; i < n; i++)
		if (buf[i] != 0)
			zero.append(buf[i]);
	show(zero);
}
void seek_max_min(QVector<int> buf, int &max, int &min)
{
	max = buf[0];
	min = buf[0];
	for (int i = 1; i < n; i++)
	{
		if (max < buf[i])
			max = buf[i];
		if (min > buf[i])
			min = buf[i];
	}
}
void multi(QVector<int> buf)
{
	int max;
	int min;
	seek_max_min(buf, max, min);
	int bool_max = 1;
	int bool_min = 1;
	int multi = 1;
	QVector<int>::iterator i = buf.begin();
	bool proverka = false;
	while (bool_max || bool_min)
	{
		if (max == *i && bool_max)
		{
			bool_max = 0;
			i++;
			continue;
		}
		if (min == *i && bool_min)
		{
			bool_min = 0;
			i++;
			continue;
		}
		if (!bool_max || !bool_min)
		{
			multi *= *i;
			proverka = true;
		}
		i++;
	}
	if (proverka)
		cout << "Multiplication: " << multi<<endl;
	else
		cout << "There wasn't element." << endl;
}
int main()
{
	srand(time(NULL));
	QVector<int> my_vector(n, 10);
	initialization(my_vector);
	without_zero(my_vector);
	multi(my_vector);
	system("pause>null");
}
