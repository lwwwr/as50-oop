// ConsoleApplication7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include "pch.h"
#include <iostream>
using namespace std;

int main()
{
	float b1 = 0.001, a1 = 100;
	float c1, w1, k1, l1, h1, f1, resh1;
	c1 = pow((a1 - b1),3);
	w1 = pow(a1, 4);
	k1 = 4 * pow(a1, 3)*b1;
	l1 = 6 * pow(a1, 2)*pow(b1, 2);
	h1 = pow(b1, 4);
	f1 = 4 * a1*pow(b1, 3);
	resh1 = (c1 - (w1 - k1 + l1)) / (h1 - f1);
	cout << "float - " << resh1 << endl;
	double b2 = 0.001, a2 = 100;
	double c2, w2, k2, l2, h2, f2, resh2;
	c2 = pow((a2 - b2), 3);
	w2 = pow(a2, 4);
	k2 = 4 * pow(a2, 3)*b2;
	l2 = 6 * pow(a2, 2)*pow(b2, 2);
	h2 = pow(b2, 4);
	f2 = 4 * a2*pow(b2, 3);
	resh2 = (c2 - (w2 - k2 + l2)) / (h2 - f2);
	cout << "double - " << resh2 << endl<<endl;

	int m, n;
	cout << "Enter n" << endl;
	cin >> n;
	cout << "Enter"<<endl;
	cin >> m;
	cout << "--m - ++n = " << --m - ++n << endl;
	cout << "m*n<n++ = " << (m * n < n++) << endl;
	cout << "n-->m++ = " << (n-- > m++) << endl;
	system("pause");


}
