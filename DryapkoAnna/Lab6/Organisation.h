#pragma once

#include <iostream>
#include <string>

using std::string;

class Organisation {
protected:
	string aName;
public:
	Organisation();
	Organisation(const Organisation&);
	Organisation(string);
	~Organisation();
	virtual void Show() = 0;
	void Add();
};

Organisation::Organisation(string name) : aName(name) {}
Organisation::Organisation(const Organisation &object) : Organisation(object.aName) {}
Organisation::Organisation() : Organisation("?????") {}
Organisation::~Organisation() {}


class List;

class Node {
private:
	friend List;
	Node(Organisation*);
	Node();
	Organisation *apElem;
	Node *apNext;
};

Node::Node() : apNext(nullptr), apElem(nullptr) {}
Node::Node(Organisation *elem) : apNext(nullptr), apElem(elem) {}

class List {
	static Node *apHead, *apTeil;
public:
	static void Show();
	static void Add(Organisation*);
};

Node* List::apHead = nullptr;
Node* List::apTeil = nullptr;

void List::Add(Organisation *object) {
	Node *pNewNode = new Node(object);
	if (!apHead) apHead = apTeil = pNewNode;
	else {
		apTeil->apNext = pNewNode;
		apTeil = apTeil->apNext;
	}
}

void List::Show() {
	Node *pTmp = apHead;
	while (pTmp) {
		pTmp->apElem->Show();
		pTmp = pTmp->apNext;
	}
}

void Organisation::Add() {
	List::Add(this);
}
