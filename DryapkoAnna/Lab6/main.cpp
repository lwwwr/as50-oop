﻿// lab6_5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include "Organisation.h"
#include "InsurComp.h"
#include "ShipComp.h"
#include "Factory.h"

int main() {
	InsurComp a("alibaba", "strit suvuruva h7", 23456);
	ShipComp b("olibobo", "strit roromaska h2", 234567);
	Factory c("poleshka", "computer Factory", 101);
	a.Add();
	b.Add();
	c.Add();
	List::Show();
}
