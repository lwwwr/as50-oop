#pragma once
#include "Organisation.h"
class Factory :
	public Organisation {
protected:
	int aQuntityOfWorking;
	string aSpecification;
public:
	Factory(string, string, int);
	Factory(const Factory&);
	Factory();
	~Factory();
	void Show() override;
};

Factory::Factory(string name, string spec, int quant) : Organisation(name), aSpecification(spec), aQuntityOfWorking(quant) {}
Factory::Factory(const Factory& object) : Factory(object.aName, object.aSpecification, object.aQuntityOfWorking) {}
Factory::Factory() : Factory("?????", "?????", 0) {}
Factory::~Factory() {}
void Factory::Show() {
	std::cout << "Factory " << aName << "; Specification: " << aSpecification << "; Workers: " << aQuntityOfWorking << std::endl;
}


