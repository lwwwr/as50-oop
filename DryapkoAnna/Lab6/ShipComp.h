#pragma once
#include "Organisation.h"
class ShipComp :
	public Organisation {
protected:
	string aLocation;
	int aPhone;
public:
	ShipComp(const ShipComp&);
	ShipComp(string, string, int);
	ShipComp();
	~ShipComp();
	void Show() override;
};

ShipComp::ShipComp(const ShipComp &object) : ShipComp(object.aName, object.aLocation, object.aPhone) {}
ShipComp::ShipComp(string name, string location, int phone) : Organisation(name), aLocation(location), aPhone(phone) {}
ShipComp::ShipComp() : ShipComp("?????", "?????", 0) {}
ShipComp::~ShipComp() {}
void ShipComp::Show() {
	std::cout << "shipbuilding company " << aName << "; location: " << aLocation << "; Phone" << aPhone << std::endl;
}