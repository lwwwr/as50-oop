#pragma once
#include "ShipComp.h"
class InsurComp : 
	public ShipComp {
public:
	InsurComp(const InsurComp&);
	InsurComp(string, string, int);
	InsurComp();
	~InsurComp();
	void Show() override;
};

InsurComp::InsurComp(const InsurComp &object) : InsurComp(object.aName, object.aLocation, object.aPhone) {}
InsurComp::InsurComp(string name, string location, int phone) : ShipComp(name, location, phone) {}
InsurComp::InsurComp() : ShipComp("?????", "?????", 0) {}
InsurComp::~InsurComp() {}
void InsurComp::Show() {
	std::cout << "insurance company " << aName << "; location: " << aLocation << "; Phone" << aPhone << std::endl;
}
