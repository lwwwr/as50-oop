#include <iostream>
#include <QVector>
#include "math.h"


void task1 () {
    using namespace std;
    cout << "task 1\n---------\n";
    QVector <double> V1 = {0.4, 0.5, 8, 0.6, -0.1, 5, 6};
    QVector <double> V2;
    QVectorIterator<double> I(V1);
    while (I.hasNext()){
        if(abs(I.next()) < 1)
            V2 += I.peekPrevious();
    }
    foreach(double el, V1)
        cout << el << " ";
    cout << endl;
    foreach(double el, V2)
        cout << el << " ";
}

void task2 () {
    using namespace std;
    cout << "\ntask 2\n---------\n";
    QVector <double> V1 = {0.4, -0.5, 8, 0.6, -0.1, 5, 6};
    QVectorIterator<double> I(V1);
    double rez = 0;
    bool exit = false;
    while(I.hasNext()){
        if(I.next() < 0){
            exit = true;
            while(I.peekNext() >= 0 && I.hasNext())
                rez += I.next();
        }
        if(exit) break;
    }
   foreach(double el, V1)
       cout << el << " ";
   cout << "\nrezult:" << rez;
}


int main() {
    task1();
    task2();
    return 0;
}
