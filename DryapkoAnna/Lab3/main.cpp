#include <QString>
#include <QTextStream>

void task();

int main(){
    task();
}

void task(){
    QTextStream cin(stdin);
    QTextStream cout(stdout);
    QString str = cin.readLine();
    QStringList list = str.split(QRegExp("\\W+"));

    int count = 0;

    cout << endl << "-----------------" << endl;
    for(int i = 0; i < list.length(); i++)
        if (list[0] == list[i])
            cout << list[i] << "(" << ++count << ")" << endl;
}
