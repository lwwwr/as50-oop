﻿#include "pch.h"
#include "Multitude.h"

int main() {
	Multitude a, b;
	cout << "input multitude: ";
	a.Input();
	cout << "multitude1: ";
	a.Print();
	cout << "input multitude: ";
	b.Input();
	cout << "multitude1: ";
	b.Print();
	cout << "set constructor c(a,s,d,f): ";
	char arr[] = { 'a', 's', 'd', 'f' };
	Multitude c(arr, 4);
	c.Print();
	cout << endl << "a + b: ";
	(a + b).Print();
	cout << endl << "a <= b: " << (a <= b);
}