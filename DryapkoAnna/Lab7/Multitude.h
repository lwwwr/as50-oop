#pragma once

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

class Multitude {
private: 
	static int MAX_SIZE;
	int aSize;
	char *apArr;
public:
	Multitude(const Multitude&);
	Multitude();
	Multitude(char*, int); // set constructor
	~Multitude();
	void Add(char);
	bool ItIs(char);
	Multitude operator + (const Multitude&); //union of set
	bool operator <= (const Multitude&); //set comparision
	void Print();
	void Input();
};

Multitude Multitude::operator + (const Multitude &obj) {
	Multitude newMultitude(*this);
	for (int i = 0; i < obj.aSize; i++)
		newMultitude.Add(obj.apArr[i]);
	return newMultitude;
}

bool Multitude::operator <= (const Multitude &obj) {
	return (aSize <= obj.aSize);
}

Multitude::Multitude(char *pArr, int size) {
	aSize = 0;
	for (int i = 0; i < size; i++)
		Add(pArr[i]);
}

int Multitude::MAX_SIZE = 225;
Multitude::Multitude() : aSize(0), apArr(nullptr) {}
Multitude::Multitude(const Multitude &obj) {
	aSize = obj.aSize;
	apArr = new char[aSize];
	for (int i = 0; i < aSize; i++)
		apArr[i] = obj.apArr[i];
}
Multitude::~Multitude() {
	delete[] apArr;
}

bool Multitude::ItIs(char el) {
	for (int i = 0; i < aSize; i++)
		if (apArr[i] == el) return true;
	return false;
}

void Multitude::Add(char el) {
	if (ItIs(el)) return;
	char *pNewArr = new char[aSize + 1];
	for (int i = 0; i < aSize; i++)
		pNewArr[i] = apArr[i];
	pNewArr[aSize++] = el;
	delete[] apArr;
	apArr = pNewArr;
}

void Multitude::Print() {
	for (int i = 0; i < aSize; i++)
		cout << apArr[i] << " ";
	cout << endl;
}

void Multitude::Input() {
	char el;
	while (true) {
		cin >> el;
		if (el != '.') Add(el);
		else return;
	}
}
