﻿#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::string;

struct str {
	str(string s, int c) : w(s), count(c) {};
	string w;
	int count;
};

struct word {
	word(string s, int n) {
		w = s;
		num.push_back(n);
	};
	string w;
	std::vector<int> num;
};

void finder(const std::vector<str> &V, std::ifstream &fin ) {
	using std::vector;
	using std::ifstream;
	fin.seekg(0, std::ios_base::beg);
	vector<word> W;
	string S;
	while (!fin.eof()) {
		fin >> S;
		bool l = false;
		for (int j = 0; j < V.size(); j++)
			if (V[j].w.find(S) != std::string::npos) {
				for (int i = 0; i < W.size(); i++)
					if (S == W[i].w) {
						W[i].num.push_back(V[j].count);
						l = true;
						break;
					}
				if (l) break;
				word elem(S, V[j].count);
				W.push_back(elem);
			}
	}
	for (int i = 0; i < W.size(); i++) {
		std::cout << W[i].w << " - ";
		for (int j = 0; j < W[i].num.size(); j++) std::cout << W[i].num[j] << " ";
		std::cout << std::endl;
	}
}

void task() {
	using namespace std;
	setlocale(LC_ALL, "rus");
	ifstream fin("InPut.txt");
	if (!fin.is_open()) {
		cout << ":(";
		return;
	}
	vector <str> V;
	int i = 0;
	string s;
	while (!fin.eof()) {
		i++;
		getline(fin, s);
		str S(s, i);
		V.push_back(S);
	}
	for (i = 0; i < V.size(); i++) cout << V[i].count << " - " << V[i].w << endl;
	cout << endl << endl;

	finder(V, fin);
	fin.close();
}

int main() {
	task();
}