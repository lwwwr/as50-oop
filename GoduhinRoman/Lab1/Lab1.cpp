#include <iostream>
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	cout << " \t\tExercise 1 " << endl;
	double a = 1000, b = 0.0001, dd;
	dd = (pow((a - b), 3) - (pow(a, 3) - 3 * pow(a, 2)*b)) / (pow(b, 3) - 3 * a*pow(b, 2));
	cout << " With double: " << dd << endl;
	float a1 = 1000, b1 = 0.0001, ff;
	ff = (pow((a1 - b1), 3) - (pow(a1, 3) - 3 * pow(a1, 2)*b1)) / (pow(b1, 3) - 3 * a1*pow(b1, 2));
	cout << " With float: " << ff << endl;
	cout << " \t\tExercise 2 " << endl;
	int m1 = 0, n1 = 0, m2 = 0, n2 = 0, m3 = 0, n3 = 0, first, second, third;
	first = --m1 - ++n1;
	second = m2 * n2 < n2++;
	third = n3-- > m3++;
	cout << " first case:" << first << "  " << m1 << "-" << n1 << endl;
	cout << " second case:" << second << "    " << m2 << "<" << n2 << endl;
	cout << " third case:" << third << "    " << m3 << ">" << n3 << endl;
	system("pause>nul");
}