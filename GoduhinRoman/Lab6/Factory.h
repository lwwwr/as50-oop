#pragma once
#include "Ships.h"
#include "List.h"
class Factory :public Ships {
protected:
	std::string department;
public:
	Factory();
	Factory(
		std::string,
		std::string,
		std::string,
		std::string,
		std::string
	);
	void Add();
	void Show();
	Factory(const Factory&);
	~Factory();
};
Factory::Factory() {
	department = "UndefinedDepartment";
};
Factory::Factory(std::string _product, std::string _country, std::string _affiliation, std::string _Purpose, std::string _department)
	:Ships(_product, _country, _affiliation, _Purpose) {
	department = _department;
};
void Factory::Show() {
	std::cout << "Factory:\n\t" << organization::country << "  " << organization::product << "  " << organization::affiliation << "  |  Purpose: " << Ships::Purpose << "  |  Department: " << department << std::endl << std::endl;
};
Factory::Factory(const Factory& _Factory) { };
Factory::~Factory() {};
void Factory::Add() {
	list *Temp = new list;
	Temp->Data = new Factory(
		product,
		country,
		affiliation,
		Purpose,
		department
	);
	Temp->Next = nullptr;
	if (List::_List.GetHead() != nullptr) {
		List::_List.GetTail()->Next = Temp;
		List::_List.SetTail(Temp);
	}
	else {
		List::_List.SetHead(Temp);
		List::_List.SetTail(Temp);
	}
}
