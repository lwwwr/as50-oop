#pragma once
#include <iostream>
#include <string>
class organization {
protected:
	std::string product;
	std::string country;
	std::string affiliation;
public: 
	organization();
	organization(
		std::string,
		std::string,
		std::string
	);
	organization(const organization&);
	virtual void Show() = 0;
	virtual void Add() = 0;
	~organization();
};
organization::organization() {
	product = "UndefinedProduct";
	country = "UndefinedCountry";
	affiliation = "UndefinedAffiliation";
}
organization::organization(std::string _product, std::string _country, std::string _affiliation) {
	product = _product;
	country = _country;
	affiliation = _affiliation;
}
organization::organization(const organization& _organization) { };
organization::~organization() { };