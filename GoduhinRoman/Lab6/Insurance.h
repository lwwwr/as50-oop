#pragma once
#include "Organization.h"
#include "List.h"
class Insurance :public organization {
protected:
	int numberOfWorkers;
public:
	Insurance();
	Insurance(
		std::string,
		std::string,
		std::string,
		int
	);
	void Show();
	void Add();
	Insurance(const Insurance&);
	~Insurance();
};
Insurance::Insurance() {
	numberOfWorkers = 0;
};
Insurance::Insurance(std::string _product, std::string _country, std::string _affiliation, int _numberOfWorkers)
	:organization(_product, _country, _affiliation) {
	numberOfWorkers = _numberOfWorkers;
};
void Insurance::Show() {
	std::cout << "Insurance:\n\t" << organization::country << "  " << organization::product << "  " << organization::affiliation << "  |  [Number of workers: " << numberOfWorkers << "]\n" << std::endl;
};
Insurance::Insurance(const Insurance& _Insurance) { };
Insurance::~Insurance() {};
void Insurance::Add() {
	list *Temp = new list;
	Temp->Data = new Insurance(
		product,
		country,
		affiliation,
		numberOfWorkers
	);
	Temp->Next = nullptr;
	if (List::_List.GetHead() != nullptr) {
		List::_List.GetTail()->Next = Temp;
		List::_List.SetTail(Temp);
	}
	else {
		List::_List.SetHead(Temp);
		List::_List.SetTail(Temp);
	}
}
#pragma once
