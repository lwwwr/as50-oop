#include <QtCore/QCoreApplication>
#include <iostream>
#include <QVector>
#include <cmath>
using namespace std;
void exercise1();
void exercise2();

int main() {
	exercise1();
	cout << endl;
	exercise2();
	system("pause>nul");
}
void exercise1() {
	cout << " Exercise1 " << endl;
	QVector<double>a;
	QVector<double>b;
	double temp = 0;
	cout << " Input array a:";
	do {
		cin >> temp;
		a.append(temp);
		if (abs(temp) <= 1) {
			b.append(temp);
			cout << temp << ' ';
		}
	} while (cin.peek() != '\n');
}

void exercise2() {
	cout << " Exercise2 " << endl;
	QVector<int>v1;
	int temp1 = 0;
	int sum = 0;
	cout << " Input array b:";
	do {
		cin >> temp1;
		v1.append(temp1);
	} while (cin.peek() != '\n');
	QVectorIterator<int>i(v1);
	while (i.hasNext()) {
		if (i.next() < 0) {
			while (i.peekNext() > 0) {
				sum += i.next();
			}
			break;
		}
	}
	cout << sum;
}