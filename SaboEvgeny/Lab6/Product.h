﻿#include <iostream>
#include <string.h>
struct P_element
{
	int amount;
	char *name = new char[20];
	int price;
	P_element *Next;
};

class Prod : public Goods
{
	static int P;  // статический компонент  данное : количество точек
	P_element *Head;
	char *p_name = new char[20];
protected:
	int p_amount;
	int p_price;
public:
	Prod()
	{
		strcpy_s(p_name, 15, "UnDefined");
		p_amount = 0;
		p_price=0;
		Head = NULL;
	}
	Prod(const char *enter_name, int enter_amount, int enter_price) :p_amount (enter_amount),
		                                                             p_price(enter_price)
	{
		strcpy_s(p_name, 15, enter_name);
	}
	~Prod()
	{
		while (Head != NULL)
		{
			P_element *temp = Head->Next;
			delete Head;
			Head = temp;
		}
	}
	void Add(const char *enter_name , int enter_amount, int enter_price)

	{
		p_amount= enter_amount;
		p_price= enter_price;
		strcpy_s(p_name, 15, enter_name);
		P_element *temp = new P_element;
		temp->name = p_name;
		temp->amount = p_amount;
		temp->price = p_price;
		temp->Next = Head;
		Head = temp;
		P++;
	}
	void show() {
		cout << endl << "Product class: " << endl;
		P_element *temp = Head;

		while (temp != NULL)
		{

			cout << "Country: " << country << endl <<
				"Date of created: " << date_created << endl <<
				"\tName of product: " << temp->name << endl <<
				"\tAmount on the scad: " << temp->amount << endl <<
				"\tCurrent price: " << temp->price << endl;
			temp = temp->Next;
		}
	}
	static int& count() { return P; }
};
