#include <iostream>
#include <cmath>
using namespace std;

void main()
{
	float a = 100.0, b = 0.001, f1, f2, f3, f4, f5, f6, F_Result;

	f1 = pow(a + b, 4.0);
	f2 = pow(a, 4.0);
	f3 = 4.0 * a * a * a * b;
	f4 = 6.0 * a * a * b * b;
	f5 = 4.0 * a * b * b * b;
	f6 = pow(b, 4.0);
	
	F_Result = (f1 - (f2 + f3)) / (f4 + f5 + f6 );
	cout << "Float result " << F_Result<<endl;

	double a1 = 100.0, b1 = 0.001, d1, d2, d3, d4, d5, d6, D_Result;

	d1 = pow(a1 + b1, 4.0);
	d2 = pow(a1, 4.0);
	d3 = 4.0 * a1 * a1 * a1 * b1;
	d4 = 6.0 * a1 * a1 * b1 * b1;
	d5 = 4.0 * a1 * b1 * b1 * b1;
	d6 = pow(b1, 4.0);
	
	D_Result = (d1 - (d2 + d3)) / (d4 + d5 + d6 );
	cout << "Double result " << D_Result << endl;

	//� ������ ������ ��� ������� ���������� ������ ������.
	//��-�� �������� ��������� �������� Float � Double.
	system("pause");
}