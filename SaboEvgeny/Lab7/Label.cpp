#include <iostream>
#include "ATD.h"
using namespace std;

void main()
{
	int size;
	cout << "Please put size of array: size= "; cin >> size;
	cout << "Create array t: \n";
	Liben_Masiven t(size);

	cout << "Show t: \n";
	t.show();
	cout << endl;

	cout << "First task: \n Create array t1 and add to the end A: \n";
	Liben_Masiven t1 = t + 'A';

	cout << "Show t1: \n";
	t1.show();
	cout << endl;

	cout << "Add to t1 B: \n";
	t1 = t1 + 'B';
	cout << "Show t1: \n";
	t1.show();
	cout << endl;

	cout << "Second task: \n We are creating array t2 and removing B from the end of array t1: \n";
	Liben_Masiven t2 = t1--;
	cout << "Show t2: \n";
	t2.show();

	cout << "\nThird task: \n We are conmparing array t2 with 's' and founding unequal elements : \n";
	t2 != 's';

	system("pause");
	
}