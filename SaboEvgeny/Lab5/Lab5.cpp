//������� - 9, ���� �������

//������� 1. ���� ������������������ �������������� �����. ���������� ������������ ����� ������������������ �� ���������� �������. 
//��� ������������� �������� � �������������� ������������������ ����������� ������������ ������ QVector.

//����� ������������������ ������ ��������� ������� ��� �������� �������� � ��������� ���������,
//����� - � ������� (� ����������� ��������� �������������� �������). 

//������� 2. ���� ������������������ �������������� �����. 

//���������� ���������: ����� ������� ��������� ������������������ �� ������� �������������� �������� (������������) �� ����� ������������������. 

#include <QtCore/QCoreApplication>
#include <QVector>
#include <ctime>
#include <iostream>
using std::cout;
using std::endl;

void fill_V(QVector<int> &buf)
{
	for (int i = 0; i < buf.size(); i++)
		buf[i] = rand() % 50 - 10;
}
void print_V(QVector<int> &buf)
{
	cout << "Show vector: " << endl;
	for (int i = 0; i < buf.size(); i++)
		cout << i + 1 << ". " << buf[i] << endl;
}
void Even_Uneven_V(QVector<int> &buf)
{
	QVector<int> Even;
	QVector<int> Uneven;
	for (int i = 0; i < buf.size(); i++)
	{
		if ((i + 1) % 2 != 0) Uneven.append(buf[i]);
		else Even.append(buf[i]);
	}
	for (int i = 0; i < buf.size(); i++)
	{
		if (i < Uneven.size()) buf[i] = Uneven[i];
		else buf[i] = Even[i - Uneven.size()];
	}
}
void abs_sum_from_first_negative(QVector<int> &buf)
{
	int sum = 0, flag = 0;
	for (QVector<int>::iterator i = buf.begin(); i < buf.end(); i++)
	{
		if (flag)
		{
			sum += abs(*i);

		}
		if (!flag && *i < 0)
		{
			flag = 1;
			sum = abs(*i);
		}
	}
	if (!flag) cout << "There wasn't negative element." << endl;
	else cout << "Abs_sum = " << sum << endl;
}
void main()
{
	srand(time(0));
	int n = 10;
	QVector<int> First_V(n);
	cout << "Capacity of vector: " << First_V.capacity() << endl << "Size of vector: " << First_V.size() << endl;
	fill_V(First_V);
	print_V(First_V);
	Even_Uneven_V(First_V);
	print_V(First_V);
	abs_sum_from_first_negative(First_V);
	system("pause>null");
}
