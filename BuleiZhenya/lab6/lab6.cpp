#include <iostream>
using namespace std;
struct NodeBase {
	NodeBase *next;
	NodeBase *prev;
	NodeBase() : next(this), prev(this) {}
	NodeBase(NodeBase *next_, NodeBase *prev_) : next(next_), prev(prev_) {
		next_->prev = prev_->next = this;
	}
	virtual ~NodeBase() {
		next->prev = prev;
		prev->next = next;
	}
};
struct Node;
class person{
protected:
	static int Id;
	char FIO[100];
public:
	person(){
		Id++;
		char *fio = new char[100];
		fio = { "-----------" };
		strcpy(FIO, fio);
		cout << "Using Base defalt constructor " << endl;
	}
	person(char *fio){
		Id++;
		strcpy(FIO, fio);
		cout << "Using Base constructor " << endl;
	}
	char* getFIO(){
		return FIO;
	}
	static int getId(){
		return Id;
	}
	virtual void Show() = 0;
	person(const person&);
	static NodeBase base;
	static void print_all();
	virtual ~person();
};
NodeBase person::base;

struct Node : public NodeBase {
	Node() : NodeBase() {}
	Node(NodeBase *next_, NodeBase *prev_, person *person_)
		: NodeBase(next_, prev_), person(person_) {}
	person *person;
};
void person::print_all() {
	NodeBase *current = base.next;
	while (current != &base) {
		(static_cast<Node*>(current))->person->print_all();
		current = current->next;
	}
}
person::~person() {
	NodeBase *node = base.next;
	while (static_cast<Node*>(node)->person != this && node != &base)
		node = node->next;
	if (node != &base)
		delete node;
}
class engineer :virtual public person {
public:
	engineer() :person(){
		char *place_ = new char[50];
		place_ = { "-----------" };
		char *name_ = new char[10];
		name_ = { "Engineer" };
		char *type_ = new char[30];
		type_ = { " " };
		strcpy(name, name_);
		strcpy(type, type_);
		strcpy(place, place_);
		cout << "Using Engineer defalt constructor " << endl;
	};
	engineer(char *fio, char *type_, char *place_) :person(fio){
		char *name_ = new char[10];
		name_ = { "Engineer" };
		strcpy(name, name_);
		strcpy(type, type_);
		strcpy(place, place_);
		cout << "Using Engineer constructor " << endl;
	};
	void Show()
	{
		cout << "Name: " << name << "-" << type << endl;
		cout << "id: " << Id << endl;
		cout << "FIO: " << FIO << endl;
		cout << "Plase: " << place << endl;
	}
	char * getName(){
		char *str = new char[50];
		strcat(str, name);
		strcat(str, "-");
		strcat(str, type);
		return str;
	}
	char * getPlace(){
		return place;
	}
protected:
	char name[10];
	char type[30];
	char place[50];
};
class employee : virtual public person {
public:
	employee() :person(){
		char *position_ = new char[50];
		position_ = { "-----------" };
		char *name_ = new char[10];
		name_ = { "Employee" };
		strcpy(name, name_);
		strcpy(position, position_);
		cout << "Using Emplouee defalt constructor " << endl;
	};
	employee(char *fio, char *position_) :person(fio){
		char *name_ = new char[10];
		name_ = {"Employee" };
		strcpy(name, name_);
		strcpy(position, position_);
		cout << "Using Emplouee constructor " << endl;
	};
	void Show()
	{
		cout << "Name: " << name << endl;
		cout << "id: " << Id << endl;
		cout << "FIO: " << FIO << endl;
		cout << "Position: " << position << endl;
	}
	char * getName(){
		return name;
	}
	char * getPosition(){
		return position;
	}
protected:
	char name[10];
	char position[50];

};
class worker :virtual public person {
public:
	worker() :person(){
		char *position_ = new char[50];
		position_ = { "-----------" };
		char *place_ = new char[50];
		place_ = { "-----------" };
		char *name_ = new char[10];
		name_ = { "Worker" };
		strcpy(name, name_);
		strcpy(position, position_);
		strcpy(place, place_);
		cout << "Using Worker defalt constructor " << endl;
	};
	worker(char *fio, char *position_, char *place_) :person(fio){
		char *name_ = new char[10];
		name_ = { "Worker" };
		strcpy(name, name_);
		strcpy(position, position_);
		strcpy(place, place_);
		cout << "Using Worker constructor " << endl;
	};
	void Show()
	{
		cout << "Name: " << name << endl;
		cout << "id: " << Id << endl;
		cout << "FIO: " << FIO << endl;
		cout << "Position: " << position << endl;
		cout << "Plase: " << place << endl;
	}
	char * getName(){
		return name;
	}
	char * getPosition(){
		return position;
	}
	char * getPlace(){
		return place;
	}
protected:
	char name[10];
	char position[50];
	char place[50];
};

int person::Id = 0;
void main()
{
	employee *a = new employee("Krishtovchuk Svetlana Sergeevna", "accountant");
	a->Show();
	engineer *b = new engineer("Stepanov Artem Borisovich", "programmer", "hospital");
	b->Show();
	worker *c = new worker("Shadrin Roman Aleksandrovich", "crane operator", "building");
	c->Show();
	system("pause");
}