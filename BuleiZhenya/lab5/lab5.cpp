#include <QVector>
#include <QDebug>
#include <time.h>
#include <iostream>
using std::cout;
using std::endl;
int* FillVector(int *vector,int count);
void ShowVector(QVector<int> Vector);
QVector<int> NegativeValue(int *vector,int count,QVector<int> vector2);
QVector<int> ZeroValue(int *vector,int count,QVector<int> vector2);
QVector<int> PositiveValue(int *vector,int count,QVector<int> vector2);
void Zadanie1(const int count, QVector<int> intVector,int *vector);
void PositionNegativeElement(const int count,int *vector);
void sumElements(int *vector, int first, int second);
int main()
{
    const int count=20;
    QVector<int> intVector(count);
    int *vector = intVector.data();
    vector=FillVector(vector,count);
    cout<< "TASK 1"<<endl;
    Zadanie1(count,intVector,vector);
    cout<< "TASK 2"<<endl;
    PositionNegativeElement(count, vector);
    system("pause");
}
int* FillVector(int *vector,int count){
    srand(time(0));
    for(int i=0; i<count;i++){
        int val=rand()%50-25;
        vector[i]=val;
    }
    return vector;
}
void ShowVector(QVector<int> Vector){
   QVector<int>::const_iterator i;
     for (i = Vector.begin(); i != Vector.end(); ++i)
      {
         qDebug() << *i;
      }
}
QVector<int> NegativeValue(int *vector,int count,QVector<int> vector2){
    for(int i=0; i<count;i++){
        if(vector[i]<0) vector2.append(vector[i]);
    }
  return vector2;
}
QVector<int> ZeroValue(int *vector,int count,QVector<int> vector2){
    for(int i=0; i<count;i++){
        if(vector[i]==0) vector2.append(vector[i]);
    }
  return vector2;
}
QVector<int> PositiveValue(int *vector,int count,QVector<int> vector2){
    for(int i=0; i<count;i++){
        if(vector[i]>0) vector2.append(vector[i]);
    }
  return vector2;
}
void Zadanie1(const int count, QVector<int> intVector, int* vector){
    cout<<" Value of first vector"<<endl;
    ShowVector(intVector);
    QVector<int> NewIntVector;
    NewIntVector=NegativeValue(vector,count,NewIntVector);
    NewIntVector=ZeroValue(vector,count,NewIntVector);
    NewIntVector=PositiveValue(vector,count,NewIntVector);
    cout<<" Value of second vector"<<endl;
    ShowVector(NewIntVector);
}
void PositionNegativeElement(const int count,int *vector){
    int firstNeg=-1,secondNeg=-1;
    for( int i=0; i<count;i++){
        if(vector[i]<0) {
            firstNeg=i;
            break;
       }
    }
    for( int i=firstNeg+1; i<count;i++){
         if(vector[i]<0) {
            secondNeg=i;
            break;
        }
    }
    if(firstNeg!=-1 && secondNeg!=-1) sumElements(vector, firstNeg, secondNeg);
    else cout<<"No first or second negative number "<<endl;
}
void sumElements(int *vector, int first, int second){
  int sum=0;
  for(int i=first+1;i<second;i++ ){
      sum+=vector[i];
  }
  cout<<"The sum of the elements located between the first and second negative elements: "<<sum<<endl;
}
