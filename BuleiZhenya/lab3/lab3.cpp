#include <QString>
#include <QVariant>
#include <QTextStream>
QTextStream cout(stdout);
QTextStream cin(stdin);
int MaxWord(QString str);
void MinWord(QString str, int Maxlenth);
int CountWord(QString str, int lenth);
int main() {
    QString str;
    str = cin.readLine();
    if(str.endsWith("."))
    {
      int Maxlenth;
      Maxlenth = MaxWord(str);
      MinWord(str, Maxlenth);
    }
    return 0;
}
int MaxWord(QString str){
    int max = 0;
    int i = 0;
    int endWord;
    int count = 0;
    while (i<str.length()+1){
        if (str[i] != ' ' && str[i] != '.')
                count++;
        else {
            if (max < count && count!=0){
                max = count;
                endWord = i-1;
            }
            count = 0;
        }
        i++;
    }
        cout << " Max Word : ";
        int countWord=CountWord(str, max);
        if (countWord > 1) cout << countWord<<" words";
        else cout<<str.mid(endWord-max+1,max);
        cout << endl;
        return max;
    }
void MinWord(QString str, int Maxlenth){
    int min = Maxlenth;
    int i = 0;
    int endWord;
    int count = 0;
    while ( i<str.length()+1){
        if (str[i] != ' ' && str[i] != '.')
            count++;
        else {
            if (min > count && count!=0){
                min = count;
                endWord = i-1;
            }
            count = 0;
        }
        i++;
    }
    cout << " Min Word : ";
    int countWord=CountWord(str, min);
    if (countWord > 1) cout << countWord<< " words";
    else{
        cout<<str.mid(endWord-min+1,min);
    }
    cout << endl;
}
int CountWord(QString str, int lenth){
    int i = 0;
    int count = 0;
    int Number=0;
    while (i<str.length()+1){
        if (str[i] != ' ' && str[i] != '.')
            count++;
        else {
            if (lenth == count){
                Number++;
            }
            count = 0;
        }
        i++;
    }
    return Number;
}
