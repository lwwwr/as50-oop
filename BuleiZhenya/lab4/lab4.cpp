#include <iostream>
#include <fstream>
#include <string>
bool LineComment(std::ifstream & in);
bool MultilineComment(std::ifstream & in);
void flushLine(std::string & line, std::ofstream & out);
void File(std::string fileName);
int main()
{
	File("D:\\in.cpp");
	return 0;
}
bool LineComment(std::ifstream & in)
{
	char c;
	while (in.get(c))
		if (c == '\n')
			return false;
	return true;
}
bool MultilineComment(std::ifstream & in)
{
	char c;
	while (in.get(c))
	{
		if (c == '*')
		{
			if (!in.get(c))
				break;
			if (c == '/')
				return false;
		}
	}
	return true;
}
void flushLine(std::string & line, std::ofstream & out)
{
	static bool prevEmptyLine = false;
	for (size_t i = 0; i < line.length(); ++i)
	{
		if (!isspace(line[i]))
		{
			prevEmptyLine = false;
			out.write(line.c_str(), line.length());
			line = "";
			return;
		}
	}
	if (!prevEmptyLine)
	{
		out.put('\n');
		prevEmptyLine = true;
	}
	line = "";
}
void File(std::string fileName)
{
	std::string line;
	std::ifstream in(fileName.c_str());
	if (!in) return;
	fileName += ".nocomments.cpp";
	std::ofstream out(fileName.c_str());
	if (!out) return;
	char c;
	bool inString = false;
	while (in.get(c))
	{
		if (inString || c != '/')
		{
			line += c;
			if (c == '\n') flushLine(line, out);
			else if (c == '\"') inString = !inString;
			continue;
		}

		if (!in.get(c))
			break;

		bool end = true;
		if (c == '/')
		{
			end = LineComment(in);
			line += '\n';
			flushLine(line, out);
		}
		else if (c == '*')
		{
			end = MultilineComment(in);
			line += ' ';
		}
		else
		{
			line += '/';
			line += c;
			continue;
		}
		if (end) break;
	}
	flushLine(line, out);
	in.close();
	out.close();
}