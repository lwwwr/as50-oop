﻿#define _CRT_SECURE_NO_WARNINGS
#include<conio.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

using namespace std;
FILE* openfile(const char*, const char*);
int main()
{
	/*FILE* file;
	 const char file1[] = "d:/f.txt";
	file = openfile(file1, "r");*/

	int a = 0;
	char ch_1[20], ch_2[20];
	FILE* file_1;
	FILE* file_2;
	const char file1[] = "d:/f1.txt";
	const char file2[] = "d:/f2.txt";

	setlocale(LC_ALL, "russian");

	if ((file_1 = openfile(file1, "r")) == NULL)
	{
		cout << ("Error!!!");	return 1;
	}

	if ((file_2 = openfile(file2, "r")) == NULL)
	{
		cout << ("Error!!!");	return 1;
	}


	while (!feof(file_1) && !feof(file_2))
	{
		fgets(ch_1, 20, file_1);
		fgets(ch_2, 20, file_2);
		a++;
		if (strcmp(ch_1, ch_2))
			cout << ("\n Строка №%d в файлах различается\n", a);
	}
	cout << ("\n Завершение сравнения!!!\n");
	fclose(file_1);
	fclose(file_2);
	return 0;
}
FILE* openfile(const char* nameFile, const char* mode)
{
	FILE* f;
	if (!(f = fopen(nameFile, mode))) {
		puts("error: not exist file");
		exit(1);
	}
	return f;
}
