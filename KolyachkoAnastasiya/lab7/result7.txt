Test is starting.
IO-test.
Adding new elements in 'theFirst' list:
Enter element: a
List's content:
a
Input() and Print() is working.
<====================>

'+' test.
theFirst list contains:
List's content:
a
Enter a new element for adding in 'theFirst' list: b
theFirst list contains after adding:
List's content:
a
b
Operator '+' is working.
<=================>

'--' test.
theFirst list contains:
List's content:
a
b
Using operator '--'...
theFirst list contains after '--' operator:
List's content:
a
Operator '--' is working.
<====================>

'!=' test.
Need to add something in theSecond list
Enter new element (enter SPACE for completion): a
Enter new element (enter SPACE for completion):
<-------->
theFirst list contains:
List's content:
a
theSecond list contains:
List's content:
a
Is 'theFirst' list == 'Second'?
Yes.
Operator '!='  working.
<====================>

Tests pass very well.