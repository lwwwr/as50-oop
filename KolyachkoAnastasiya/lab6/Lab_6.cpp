﻿#include <iostream>
#include "myclass.h"

using namespace std;

void Test();

int main() {
	Test();
	return 0;
}

void test() {
	MyClass thefirst;
	MyClass thesecond = thefirst;
	cout << "Test start\n";

	cout << "IO-test.\nAdding new elements in 'theFirst' list:\n";
	try {
		thefirst.Input();
		thefirst.Print();
	}
	catch (...) {
		cout << "Input error occure.\n";
	}
	cout << "Input() and Print() is working.\n<=============>\n\n";

	cout << "'+' test.\n";
	try {
		char tmp = ' ';
		cout << “ the first list contains : \n";
			thefirst.Print();
		cout << " new element for adding in 'theFirst' list: ";
		cin >> tmp;
		thefirst + tmp;
		cout << "The First list contains after adding:\n";
		thefirst.Print();
	}
	catch (...) {
		cout << "'+' error ocure.\n";
	}
	cout << "Operator '+' is working.\n<=============>\n\n";


	cout << "'---' test.\n";
	try {
		cout << "TheFirst list contains:\n";
		thefirst.Print();
		cout << "Using operator '--'...\n";
		thefirst--;
		cout << "TheFirst list contains after '--' operator:\n";
		thefirst.Print();
	}
	catch (...) {
		cout << "'--' error ocure.\n";
	}
	cout << "Operator '--' is working.\n<===============>\n\n";

	cout << "'!=' test.\n";
	try {
		cout << " add something in Second list\n";
		char tmp = ' ';
		do {
			cout << "Enter a new element (enter SPACE for completion): ";
			cin.get(tmp);
			cin.get(tmp);
			if (tmp != ' ' && tmp != '\n')
				second + tmp;
		} while (tmp != ' ');
		cout << "<-------->\n";
		cout << "theFirst list contains:\n";
		thefirst.Print();
		cout << "thetSecond list contains:\n";
		thesecond.Print();
		cout << "Is 'theFirst' list == 'Second'?\n";
		if (thefirst != thesecond)
			cout << "No.\n";
		else
			cout << "Yes.\n";
	}
	catch (...) {
		cout << "'!=' error ocure.\n";
	}
	cout << "Operator '!='  working.\n<==============>\n\n";

	cout << "Tests pass very well.\n";
}






