#include <iostream>
 
template<typename T>
class slist {
    struct node {
        node* next;
        T     val;
    };
private:
    node* lst;
public:
    slist(void):lst(NULL){}
    ~slist(){
        this->clear();
    }
public:
    
    slist& operator + (const T& val){
        node* p = new (std::nothrow) node();
        if(p != NULL){
            p->val  = val;
            p->next = lst;
            lst = p;
        }
        return *this;
    }
 
    slist& operator -- (void){
        node* t;
        if(lst != NULL){
            t   = lst;
            lst = lst->next;
            delete t;
        }
        return *this;
    }
 
    void clear(void){
        node* t;
        while(lst != NULL){
            t   = lst;
            lst = lst->next;
            delete t;
        }
    }
 
    T& operator *(void) const { return lst->val; }
    T& operator *(void) { return lst->val; }
 
    bool empty(void) const { return (lst == NULL); }
};
 
int main(void){
    slist<char> lc;
    for(char c = 'A'; c <= 'Z'; ++c)
        lc = lc + c;
 
    while(! lc.empty()){
        std::cout << *lc;
        --lc;
    }
    return 0;
	system("pause");
}