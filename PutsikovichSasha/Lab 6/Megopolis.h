#pragma once
#include "Oblast.h"
#include "City.h"
#include "Place.h"
#include "List.h"

class Megopolis :public Place {
protected:
	std::string department;
public:
	Megopolis();

	Megopolis(
		std::string,
		std::string,
		std::string,
		std::string,
		std::string
	);
	void Add();
	void Show();
	Megopolis(const Megopolis&);
	~Megopolis();
};
Megopolis::Megopolis() {
	department = "UndefinedDepartment";
};

Megopolis::Megopolis(std::string _nameobl, std::string _people, std::string _maincity, std::string _cityofpeople, std::string _department)
	:Place(_nameobl,  _people, _maincity,  _cityofpeople) {
	department = _department;
};

void Megopolis::Show() {
	std::cout << " Name of oblast: " << Oblast::people << "   " << "People:" << Oblast::nameobl << "   " << " Main city: " << Oblast::maincity << "   " << " City of people: " << Place::cityofpeople << "   " << " Department: " << department << std::endl << std::endl;
};
Megopolis::Megopolis(const Megopolis& _Megopolis) { };
Megopolis::~Megopolis() {};
void Megopolis::Add() {
	list *Temp = new list;
	Temp->Data = new Megopolis(
		nameobl,
		people,
		maincity,
		cityofpeople,
		department
	);
	Temp->Next = nullptr;
	if (List::_List.GetHead() != nullptr) {
		List::_List.GetTail()->Next = Temp;
		List::_List.SetTail(Temp);

	}
	else {
		List::_List.SetHead(Temp);
		List::_List.SetTail(Temp);

	}
}