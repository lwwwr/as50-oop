#pragma once
#include "Oblast.h"
#include "List.h"

class City :public Oblast {
protected:
	int citypeople;
public:
	City();
	City(
		std::string,
		std::string,
		std::string,
		int 
	);
	void Show();
	void Add();
	City(const City&);
	~City();
};
City::City() {
	citypeople = 0;
};

City::City(std::string _nameobl, std::string _people, std::string _maincity, int _citypeople)
	:Oblast(_nameobl, _people, _maincity) {
	citypeople = _citypeople;
};
void City::Show() {
	std::cout << " Name of oblast: " << Oblast::people <<"   "<<  "People:" << Oblast::nameobl << "   " << "Main city:  " << Oblast::maincity << "   " << "Number of people: " << citypeople << std::endl<<std::endl;
};
City::City(const City& _City) { };
City::~City() {};
void City::Add() {
	list *Temp = new list;
	Temp->Data = new City(
		nameobl,
		people,
		maincity,
		citypeople
	);
	Temp->Next = nullptr;
	if (List::_List.GetHead() != nullptr) {
		List::_List.GetTail()->Next = Temp;
		List::_List.SetTail(Temp);

	}
	else {
		List::_List.SetHead(Temp);
		List::_List.SetTail(Temp);

	}
}