#include <iostream>
#include <QVector>
#include <math.h>

void Zad1() {
	using std::cout;
	QVector<double> Inpvect = { 0.1, 0.2, 6, 7, -5, -0.3, 3, -0.6 };
	QVector<double> result, result2;
	foreach(double element, Inpvect)
	if (fabs(element) < 1) result += element;
	else result2 += element;
	result += result2;
	cout << "zadanie 1:\ninput string: ";
	foreach(double element, Inpvect) cout << element << " ";
	cout << "\n--------------------------\noutput string: ";
	foreach(double element, result) cout << element << " ";	
}

void Zad2() {
	using std::cout;
	int result = 0, position;
	bool check = false;
	QVector<int> Inpvect = { -3, -5, 4, 5, -8, 7, 9, -6, -7 };
	cout << "\n\nzadanie 2:\n";
	if (Inpvect.isEmpty()) return;
	for (int i = Inpvect.indexOf(Inpvect.last() - 1); i >= 0; i--) {
		if (Inpvect[i] > 0) {
			position = Inpvect[i];
			check = true;
			break;
		}
		if (!check) {
			cout << "There is no positive elements in the string";
			return;
		}
		for (int i = 0; i <= Inpvect.indexOf(position); i++) result += Inpvect[i];
		foreach(int element, Inpvect) cout << element << " ";
		cout << "\nresult: " << result;
	}
}

int main() {
	Zad1();
	Zad2();
}