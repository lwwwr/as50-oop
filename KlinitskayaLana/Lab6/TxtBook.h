#pragma once
#include "Book.h"
class TxtBook :
	public Book
{
protected:
	string aSpecific;
public:
	TxtBook(string, int, string, string);
	TxtBook(const TxtBook&);
	TxtBook();
	void ShowElem() override;
	~TxtBook();
};

TxtBook::TxtBook(string publHouse, int year, string autor, string specific) :
	Book(publHouse, year, autor),
	aSpecific(specific)
{}
TxtBook::TxtBook(const TxtBook &obj) : TxtBook(obj.aPublHouse, obj.aYear, obj.aAutor, obj.aSpecific) {}
TxtBook::TxtBook() : TxtBook("???", 0, "???", "???") {}

void TxtBook::ShowElem() {
	cout << "(TxtBook) publ. house: " << aPublHouse
		<< "year: " << aYear << " autor: " << aAutor
		<< "specification: " << aSpecific << endl;
}
TxtBook::~TxtBook() { }