#pragma once
#include "PrEdit.h"
class PrEdit;
class List {
private: friend PrEdit;
	class Node {
	public:
		Node(PrEdit*);
		Node();
		PrEdit *aElem;
		Node *apNext;
	};
static Node *apHead;
static void AddInList(PrEdit*);
public:
	static void ShowList();
	List();
	~List();
};
List::Node::Node(PrEdit *elem) : aElem(elem), apNext(nullptr) {}
List::Node::Node() : apNext(nullptr), aElem(nullptr) {}
List::Node* List::apHead = nullptr;

void List::AddInList(PrEdit *pElem) {
	Node *pNode = new Node(pElem);
	if (!apHead) apHead = pNode;
	else {
		pNode->apNext = apHead;
		apHead = pNode;
	}
}
void List::ShowList() {
	for (Node *pCur = apHead; pCur; pCur = pCur->apNext)
		pCur->aElem->ShowElem();
}
List::List() {}
List::~List() {}