﻿#include "List.h"
#include "PrEdit.h"
#include "Book.h"
#include "Magazine.h"
#include "TxtBook.h"

int main() {
	Book a("onepiece ", 1988, "Oda");
	Magazine b("archedy ", 2018, "Auto");
	TxtBook c("onepiece ", 2015, "Krilliant B.A.", "English language");
	Book d(a);
	List::ShowList();
}