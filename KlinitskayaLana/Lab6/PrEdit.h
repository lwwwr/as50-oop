#pragma once
#include "List.h"
#include <string>
#include <iostream>
using std::string;
using namespace std;

class List;
class PrEdit {
protected:
	string aPublHouse;
	int aYear;
	void Add();
public:
	PrEdit();
	PrEdit(const PrEdit&);
	PrEdit(string, int);
	~PrEdit();
	virtual void ShowElem() = 0;
};
PrEdit::PrEdit(string publHouse, int year) : aPublHouse(publHouse), aYear(year) {
	Add();
}
PrEdit::PrEdit(const PrEdit &obj) : PrEdit(obj.aPublHouse, obj.aYear) {}
PrEdit::PrEdit() : PrEdit("???", 0) {}
PrEdit::~PrEdit() {}
void PrEdit::Add() {
	List::AddInList(this);
}