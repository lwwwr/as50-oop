#include<iostream>
using namespace std;

int main() {
	float a = 1000.0, b = 0.0001;
	float funcFloat;
	funcFloat = (pow((a - b), 3) - (pow(a, 3) - 3 * a * pow(b, 2))) / (pow(b, 3) - 3 * pow(a, 2) * b);
	double A = 1000.0, B = 0.0001;
	double funcDouble;
	funcDouble = (pow((A - B), 3) - (pow(A, 3) - 3 * A * pow(B, 2))) / (pow(B, 3) - 3 * pow(A, 2) * B);
	cout << "1 parth" << endl;
	cout << "Result in float type : " << funcFloat << endl << "Result in double type : " << funcDouble << endl;
	cout << endl << "2 parth" << endl;
	int m, n;
	cout << "enter m : "; cin >> m;
	cout << "enter n : "; cin >> n;
	cout << "m - ++n : " <<m - ++n << endl; //here 2 steps: 1st - ++n , 2nd - m - n; so it's equals m - (++n)
	int secondOper = ++m > --n; // ++m and --n are prefix operations, so they are perform before comparison
	cout << "++m > --n :" << secondOper << endl; // function compares values. if true - 1, false - 0.
	int thirdOper = --n < ++m; // already -- and ++ are prefix operation.
	cout << "--n < ++m :" << thirdOper << endl;
	system("pause");
	return 0;
}