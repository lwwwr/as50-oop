#pragma once
#include <cstring>
#include <iostream>

const int MAX_SIZE = 225;

class Masssort {
private:
	char *apArr;
	static const int MAX;
	int aSize;
	void Copy(const char*, int);
	void CallBack(char);
public:
	Masssort();
	Masssort(const Masssort&);
	Masssort(const char*, int);
	~Masssort();
	void InPut();
	void Print();
	bool operator > (char ch);
	Masssort& operator * (const Masssort&);
	bool operator < (const Masssort&);
};

bool Masssort::operator<(const Masssort&obj) {
	if (aSize < obj.aSize) return false;
	bool ok = false;
	for (int i = 0; i < obj.aSize; i++) {
		for (int j = 0; j < aSize; j++) {
			if (obj.apArr[i] == apArr[j]) {
				ok = true;
				break;
			}
		}
		if (ok) ok = false;
		else return false;
	}
	return true;
}

Masssort& Masssort::operator*(const Masssort& obj) {
	Masssort *newArr = new Masssort;
	int k = 0;
	for (int i = 0; i < obj.aSize; i++)
		for (int j = 0; j < aSize; j++) {
		if (obj.apArr[i] == apArr[j]) {
			newArr->CallBack(apArr[j]);
			break;
			
		}
		
	}
	return *newArr;
}

bool Masssort::operator>(char ch) {
	for (int i = 0; i < aSize; i++)
		if (ch == apArr[i]) return true;
	return false;
}

Masssort::Masssort(const char *pArr, int size) : apArr(nullptr), aSize(size) {
	Copy(pArr, size);
};

void Masssort::Copy(const char *pArr, int size) {
	if (size > MAX) return;
	if (apArr) delete apArr;
	aSize = size;
	apArr = new char[size];
	for (int i = 0; i < size; i++)
		apArr[i] = pArr[i];
}

const int Masssort::MAX = MAX_SIZE;

Masssort::Masssort() : apArr(nullptr), aSize(0) {};

Masssort::Masssort(const Masssort &obj) {
	Copy(obj.apArr, obj.aSize);
}

Masssort::~Masssort() {
	delete[] apArr;
}

void Masssort::InPut() {
	char ch;
	while (aSize < MAX) {
		std::cin >> ch;
			if (ch == '.') break;
			else CallBack(ch);
	}
}

void Masssort::CallBack(const char ch) {
	if ((*this) > ch) return;
	char *pNewArr = new char[aSize + 1];
	for (int i = 0; i < aSize; i++)
		pNewArr[i] = apArr[i];
	pNewArr[aSize++] = ch;
	delete[] apArr;
	apArr = pNewArr;
}

void Masssort::Print() {
	for (int i = 0; i < aSize; i++)
		std::cout << apArr[i] << " ";
	std::cout << std::endl;
}