﻿#include <QString>
#include <QTextStream>
#include <iostream>

QTextStream in(stdin),
            out(stdout);

using std :: cout;
using std :: cin;
using std :: endl;

bool IsPol(QString str1){
    QString str2(str1);
    std::reverse(str2.begin(), str2.end());
    return str1.toLower() == str2.toLower();
}

void task(QString str){
    bool pol = false;
    QStringList list = str.split(QRegExp("\\W+"));
    cout << endl << "polindroms: " << endl;
    for(int i = 0; i < list.count(); i++)
        if(IsPol(list[i])){
            out << list[i] << endl;
            pol = true;
        }
    if(!pol)
        cout << "not polindroms :(";
}

int main()
{
    setlocale(LC_ALL, "rus");
    QString str;
    str = in.readLine();
    task(str);
    return 0;
}
