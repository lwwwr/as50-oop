﻿// Lab7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
 #include <list>
 #include <vector>
using namespace std;
class ATD {
	protected:
		virtual void showInf() = 0;
};
class Res : public ATD {
	protected:
		public:
			list<char> El = { 'q','w','e','r','t' };
			void showInf() override {
				cout << "List: ";
				for (char n : El) {
					cout << n << " ";
				}
				cout << endl;
			}
			char& operator [] (const int index);
			list<char>& operator +(const list<char> second) {
				auto iter2 = second.begin();
				for (char n : second) {
					El.emplace_back(*iter2);
					++iter2;
				}
				return El;
			}
			bool operator !=(const list<char> second) {
				auto iter = El.begin();
				auto iter2 = second.begin();
				for (char n : second) {
					if (*iter == *iter2) {
						return false;
						
					}
				}
				return true;
				
			}
			int begin;
};
char& Res::operator[](const int index) {
	auto iter = El.begin();
	for (int i = 0; i < index; i++) {
		iter++;
		
	}
	return *iter;
	
}
int main() {
	Res List;
	Res result;
	list<char> second = { 'z','x','c','v' };
	list<char> me = { 'q','w','e','r','t','z','x','c','v' };
	List.showInf();
	List + second;
	List.showInf();
	if (List != second) {
		cout << "yes" << endl;
		
	}
	else
		 cout << "no" << endl;
	if (List != me) {
		cout << "yes" << endl;
		
	}
	else
		 cout << "no" << endl;
	cout << List[6] << endl;
	system("pause");
	return 0;
}