#include<cstdlib>
#include<iostream>
#include<math.h>
int main()
{
	using namespace std;
	float a1, b1, c1;
	double a2, b2, c2;
	cout << "Enter a1:";
	cin >> a1;
	cout << "Enter b1:";
	cin >> b1;
	c1 = ((pow(a1 + b1, 3)) - (pow(a1, 3) + 3 * b1*(pow(a1, 2)))) / (3 * a1*(pow(b1, 2)) + (pow(b1, 3)));
	cout << "\nFloat:";
	printf("%.10f", c1);
	cout << "\nEnter a2:";
	cin >> a2;
	cout << "Enter b2:";
	cin >> b2;
	c2 = ((pow(a2 + b2, 3)) - (pow(a2, 3) + 3 * b2*(pow(a2, 2)))) / (3 * a2*(pow(b2, 2)) + (pow(b2, 3)));
	cout << "\nDouble:";
	printf("%.10f", c2);
	cout << "\n";
	system("PAUSE");
	return 0;
}
