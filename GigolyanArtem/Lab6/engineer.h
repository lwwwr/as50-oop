#pragma once
#include "engineer.h"
 class engineer :
	public worker{
	protected:
		int aCategory;
		public:
			engineer(const engineer&);
			engineer();
			engineer(string, int, int);
			~engineer();
			void Print() override;
			};
engineer::engineer(const engineer &el) : engineer(el.aName, el.aWorkExperience, el.aCategory) {}
engineer::engineer(string name, int experience, int category) : worker(name, experience), aCategory(category) {}
engineer::engineer() : engineer("???", 0, 0) {}
engineer::~engineer() {}
void engineer::Print() {
	cout << "engineer. " << "name: " << aName << "; work Experince: " << aWorkExperience <<
		"; category: " << aCategory << endl;
}
