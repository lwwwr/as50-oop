#include <iostream>
#include <string>
#define _CRT_SECURE_NO_WARNINGS
using namespace std;
class Arr {
public:
	Arr() {
		id++;
		const char* str_ = new char[15];
		str_ = { "----------\0" };
		strcpy_s(str, str_);
		this->head = this;
		cout << "Using defalt constructor " << endl;
	}
	Arr(char* str_) {
		id++;
		strcpy_s(str, str_);
		this->head = this;
		cout << "Using defalt constructor with paraments " << endl;
	}
	Arr(const Arr& obj) {
		id = obj.id;
		strcpy_s(str, sizeof(obj.str), obj.str);
		head = obj.head;
		next = obj.next;
	}
	Arr() {
	}
	void Add(Arr* obj)
	{
		if (obj->id < maxSize) {
			obj->head = this->head;
			this->next = obj;
			obj->id = (this->id) + 1;
		}
		else cout << "Array is full" << endl;
	}
	void Show() {
		cout << id << endl << str << endl;
	}
	void ShowAll() {
		cout << id << endl << str << endl;
		if (next) {
			next->ShowAll();
		}
	}
	void input() {
		char* str_ = new char[100];
		cin >> str_;
		strcpy_s(str, str_);
	}
	Arr& operator=(const Arr& obj) {
		id = obj.id;
		strcpy_s(str, obj.str);
		head = obj.head;
		next = obj.next;
		return *this;
	}
	Arr operator+(char symbol) {
		Arr obj;
		if (strlen(str) + 1 < maxLeng)
		{
			strcpy_s(obj.str, str);
			obj.str[strlen(str)] = symbol;
			obj.str[strlen(str) + 1] = '\0';
		}
		else
		{
			cout << "Array crowde ";
		}
		return obj;
	}
	friend void operator* (const Arr obj1, const Arr obj2) {
		for (int i = 0; i < strlen(obj1.str); i++) {
			for (int j = 0; j < strlen(obj2.str); j++) {
				if (obj1.str[i] == obj2.str[j]) {
					cout << obj1.str[i]; i++;
				}
			}
		}
	}
	operator int() {
		return strlen(str);
	}
private:
	int id = 0;
	static int maxSize;
	const int maxLeng = 100;
	char str[100];
	Arr* next = NULL;
	Arr* head = NULL;
};
int Arr::maxSize = 5;
void main() {
	char str1[] = { "Hello" };
	char str2[] = { "dear" };
	char str3[] = { "friend" };
	Arr obj1(str1), obj2(str2), obj3(str3), obj4;
	obj1.Add(&obj2);
	obj1.ShowAll();
	obj4.Show();
	obj4 = obj3;
	obj4.Show();
	cout << "obj3 + !: ";
	obj3=obj3 + '!';
	obj3.Show();
	cout << "obj1*obj3: ";
	obj1* obj3;
	int k = obj1;
	cout << "\npower: " << k << endl;
	system("pause");
}