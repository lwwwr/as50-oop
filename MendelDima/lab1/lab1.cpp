#include <iostream>

using namespace std;

int main(){
    cout.precision(10);
	float ResultFloat, f1, f2, f3, f4, f5, f6, f7, a1=1000, b1=0.0001;
	double ResultDouble, d1, d2, d3, d4, d5, d6, d7, a2=1000, b2=0.0001;
	f1=(a1+b1)*(a1+b1)*(a1+b1);
	f2=a1*a1*a1;
	f3=f1-f2;
	f4=3*a1*b1*b1;
	f5=b1*b1*b1;
	f6=3*a1*a1*b1;
	f7=f4+f5+f6;
	ResultFloat=f3/f7;
	d1=(a2+b2)*(a2+b2)*(a2+b2);
	d2=a2*a2*a2;
	d3=d1-d2;
	d4=3*a2*b2*b2;
	d5=b2*b2*b2;
	d6=3*a2*a2*b2;
	d7=d4+d5+d6;
	ResultDouble=d3/d7;
	cout<<"result float = "<<ResultFloat<<endl;
	cout<<"result double = "<<ResultDouble<<endl;
	int n, m, r1, r2, r3;
    cout<<"Enter n: ";
	cin >> n;
	cout <<"Enter m: ";
	cin>>m;
	r1 = n++*m;
	r2 = n++<m;
	r3 = n-->m;
	cout <<"n++*m = "<< r1 << endl;
	cout <<"n++<m = "<< r2 << endl;
	cout <<"n-->m = "<< r3 << endl;
return 0;
}

