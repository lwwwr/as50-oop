#include <iostream>
using namespace std;
void Words(char* str) {
    char* start = str;
    int c = 0;    
    while (true) {
       if (*str >= '0' && *str <= '9') {
            c++;
            str++;
            continue;
        }
        if (*str == ' ' || *str == '\0') {
            if (c == 1) { 
                while (start < str) {  
                    cout << *start;
                    start++;
                }
                cout <<"; ";
            }
            if (*str == '\0')  
                break;
            Words(++str); 
            break;
        }
        str++;
    }
}
int main()
{
    char str[256];
    cout << "Enter words: ";
    cin.getline(str, 255);
    str[255] = '\0';
    cout << endl;
    cout<<"Received string (255 characters): "<<str<<endl;
    cout << endl;
    cout<<"Result: ";
    Words(str);

}