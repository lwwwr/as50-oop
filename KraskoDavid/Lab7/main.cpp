﻿#include <iostream>
#include "myclass.h"

using namespace std;

void Test();

int main(){
	Test();
	return 0;
}

void Test(){
	MyClass first;
	MyClass second = first;
	cout << "Test is starting.\n";


	//input test
	cout << "IO-test.\nAdding new elements in 'First' list:\n";
	try{
		first.Input();
		first.Print();
	}catch(...){
		cout << "Input error occure.\n";
	}
	cout << "Input() and Print() is working.\n<====================>\n\n";


	//test operator "+"
	cout << "'+' test.\n";
	try{
		char tmp = ' ';
		cout << "First list contains:\n";
		first.Print();
		cout << "Enter a new element for adding in 'First' list: ";
		cin >> tmp;
		first + tmp;
		cout << "First list contains after adding:\n";
		first.Print();
	}catch(...){
		cout << "'+' error occure.\n";
	}
	cout << "Operator '+' is working.\n<====================>\n\n";


	//test operator "--"
	cout << "'--' test.\n";
	try{
		cout << "First list contains:\n";
		first.Print();
		cout << "Using operator '--'...\n";
		first--;
		cout << "First list contains after '--' operator:\n";
		first.Print();
	}catch(...){
		cout << "'--' error occure.\n";
	}
	cout << "Operator '--' is working.\n<====================>\n\n";


	//test operator "!="
	cout << "'!=' test.\n";
	try{
		cout << "Need to add something in Second list\n";
		char tmp = ' ';
		do{
			cout << "Enter a new element (enter SPACE for completion): ";
			cin.get(tmp);
			cin.get(tmp);
			if(tmp!=' ' && tmp !='\n')
				second + tmp;
		} while(tmp!=' ');
		cout << "<-------->\n";
		cout << "First list contains:\n";
		first.Print();
		cout << "Second list contains:\n";
		second.Print();
		cout << "Is 'First' list == 'Second'?\n";
		if(first!=second)
			cout << "No.\n";
		else
			cout << "Yes.\n";
	}catch(...){
		cout << "'!=' error occure.\n";
	}
	cout << "Operator '!=' is working.\n<====================>\n\n";

	cout << "Tests pass successfuly.\n";
}
