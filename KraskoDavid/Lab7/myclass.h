﻿#ifndef MYCLASS_H
#define MYCLASS_H
#include <iostream>
#include <list>
#include <iterator>
using namespace std;

class MyClass
{
private:
	list<char> myList;
public:
	MyClass();
	MyClass(MyClass &obj);
	~MyClass();
	void Input();
	void Print();
	void operator + (char a);
	MyClass* operator-- (int i);
	bool operator != (MyClass &obj);
};

#endif // MYCLASS_H

MyClass::MyClass()
{

}

MyClass::MyClass(MyClass &obj)
{
	myList = obj.myList;
}

MyClass::~MyClass()
{
	list <char> :: iterator i = myList.end();
	while(i != myList.begin()){
		myList.pop_back();
		i--;
	}
}

void MyClass::Input()
{
	char tmp = ' ';
	cout << "Enter an element: ";
	cin >> tmp;
	myList.push_back(tmp);
}

void MyClass::Print()
{
	cout << "List's content:\n";
	list <char> :: iterator i = myList.begin();
	while(i != myList.end()){
		cout << *i << endl;
		i++;
	}
}

void MyClass::operator + (char a)
{
	myList.push_back(a);
}

MyClass* MyClass::operator-- (int i)
{
	if(!this->myList.empty())
		this->myList.pop_back();
	else
		cout << "list is already empty.\n";
	return this;
}

bool MyClass::operator !=(MyClass &obj)
{
	if(this->myList != obj.myList)
		return true;
	else
		return false;
}
