﻿#ifndef PRODUCT_H
#define PRODUCT_H
//base
#include <string>
#include <iostream>
#include <map>

using namespace std;

static multimap<int,string> productList;

class Product
{
protected:
	string name;
	string madeIn;
	enum eType {eProduct=0,eGood=1, eMilk=2, eToy = 3} currType;
public:
	Product(string = "NO_SET", string = "NO_SET");
	virtual ~Product();
	virtual void Add() = 0;
	virtual string Show() = 0;
	void ShowList();
};

Product::Product(string _name, string _madeIn)
{
	name = _name;
	madeIn = _madeIn;
	currType = eProduct;
}

Product::~Product()
{
	name.clear();
	madeIn.clear();
	delete this;
}

void Product::Add()
{
}
void Product::ShowList()
{
	for(int i = 1; i< 4; i++){
		switch(i){
		case eGood: cout << "Goods: \n"; break;
		case eToy: cout << "Toys: \n"; break;
		case eMilk: cout << "Diary products: \n"; break;
		}
		for(auto item : productList)
		{
			if(item.first == i)
				cout << item.second << endl;
		}
		cout << "<!=================!>\n";
	}

}
#endif // PRODUCT_H
