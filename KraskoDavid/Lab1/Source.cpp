﻿#include<iostream>
using namespace std;

int main() {
	cout << "Exercise 1." << endl;
	float ResInFloat, af = 100, bf = 0.001;
	double ResInDouble, ad = 100, bd = 0.001;
	ResInFloat = (pow(af + bf, 4) - (pow(af, 4) + 4 * af*af*af*bf)) / 6*af*af*bf*bf + 4*af*bf*bf*bf + pow(bf,4);
	ResInDouble = (pow(ad + bd, 4) - (pow(ad, 4) + 4 * ad*ad*ad*bd)) / 6 * ad*ad*bd*bd + 4 * ad*bd*bd*bd + pow(bd, 4);
	cout << "Result in float type : " << ResInFloat << "\nResult in double type : " << ResInDouble << endl;

	cout << "Exercise 2. Here 0 equals false and 1 eq true" << endl;
	int n = 3, m = 3, z;
	cout << "n+++m :" << ++n*++ m << endl; 
	z = m++ < n; 
	cout << "m++ < n = " << z << endl;
	z = n++ > m;
	cout << "n++ > m = " << z << endl;
	system("pause");
}