#include <QString>
#include <QTextStream>
#include <iostream>
using namespace std;

QTextStream read(stdin), write(stdout);

bool Search(QString StrIn)
{
    QString StrOut(StrIn);
    reverse(StrOut.begin(), StrOut.end());
    return StrIn.toLower() == StrOut.toLower();
}

void Output(QString Str)
{
    bool Found = false;
    QStringList List = Str.split(QRegExp("\\W+"));
    cout << "All palindromes found in the text:" << endl;
    for (int i = 0; i < List.count(); i++)
        if (Search(List[i]))
        {
            write << List[i] << endl;
            Found = true;
        }
    if (!Found)
        cout << "No palindromes found";
}

int main()
{
    cout << "Enter the text:" << endl;
    QString Text;
    Text = read.readLine();
    Output(Text);
    return 0;
}
