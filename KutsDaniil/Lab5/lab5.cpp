#include <QCoreApplication>
#include <QVector>
#include <QtDebug>

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "Russian");
    QCoreApplication a(argc, argv);
    srand(time(NULL));
    QVector <double> in, out;
    for (size_t i = 0; i < rand() % 9 + 4; i++) {
        in << ((rand() %18- 9) + ((rand() %18-9) + 1) * 0.1)* ((rand() % 9 + 1) > 5 ? -1 : 1)  * ((rand() % 5 + 1) > 4 ? 0 : 1);
    }
    QVector<double>::const_iterator ci;
    qDebug() << "Задание 1. Ввод:" << in;
    for (ci = in.constBegin(); ci != in.constEnd(); ++ci) {
        if (*ci >= .0) { out.push_front(*ci); continue; }
        out.push_back(*ci);
    }
    for(ci = out.constBegin(); ci != out.constEnd(); ++ci) { qDebug() << *ci; }

    QList <double> list;
    for (size_t i = 0; i < rand() % 9 + 4; i++) {
        list << (rand() % 9 + (rand() % 9 + 1) * 0.1)* ((rand() % 9 + 1) > 5 ? -1 : 1);
    }
    qDebug() << "Задание 2. Ввод: " << list;
    QMutableListIterator<double>  mlit(list);
    double s;
    while (mlit.hasNext()) {
        if (mlit.next() <= 0) { mlit.remove(); continue; }
        mlit.remove();
        break;
    }
    mlit.toFront(); ++mlit.next();
    while (mlit.hasNext()) {
        s += mlit.next();
    }
    qDebug() << "Сумма: " << s;
    return a.exec();
}