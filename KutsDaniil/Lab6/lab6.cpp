#include<iostream>
using namespace std;

static List list;
static void print_all();
virtual void Show() {};
Print(const Print&) {}
virtual ~Print();

struct List
{
	List * next;
	List * prev;
	List() : next(this), prev(this) {}
	List(List *nxt, List *prv) : next(nxt), prev(prv)
	{
		nxt->prev = prv->next = this;
	}
	
	virtual ~List()
	{
		next->prev = prev;
		prev->next = next;
	}
};
struct Node;

class Print
{
public:
	Print(const char* name, const char * sign, int number = 0, int sum = 0)
	{
		strcpy_s(Name, name);
		strcpy_s(Sign, sign);
		Sum = sum;
		Number = number;
		cout << endl << "Using List constructor" << endl;
	}
	char* GetName()
	{
		return Name;
	}
	int GetSum()
	{
		return Sum;
	}
	char* GetSign()
	{
		return Sign;
	}
	int GetNumber()
	{
		return Number;
	}
protected:
	int Number;
	char Name[100];
	int Sum;
	char Sign[100];
	
};
List Print::list;
struct Point : public List
{
	Point() : List() {}
	Point(List *nxt, List *prv, Print *z_)
		: List(nxt, prv), z(z_) {}
	Print * z;
};

void Print::print_all()
{
	List * direction = list.next;
	while (direction != &list)
	{
		(static_cast<Point*>(direction))->z->print_all();
		direction = direction->next;
	}
}

Print::~Print()
{
	List * point = list.next;
	while (static_cast<Point*>(point)->z != this && point != &list)
		point = point->next;
	if (point != &list)
		delete point;
}

class Receipt : virtual public Print
{
public:		
	Receipt(const char* name, const char * sign, int number = 0, int sum = 0) : Print(name, sign, number, sum)
	{
		cout << "Receipt Constructor" << endl;
	}
	void Show()
	{
		cout << endl << "Receipt" << endl;
		cout << "�: " << Number << endl;
		cout << "Type of payment: " << Name << endl;
		cout << "Sum: " << Sum << endl;
		cout << "Sign: " << Sign << endl;
	}
};

class Invoice : public Print
{
public:
	Invoice(const char* name, const char * sign, const char * date, int quantity, int number = 0) : Print(name, sign, number)
	{
		strcpy_s(Date, date);
		Quantity = quantity;
		cout << "Invoice constructor" << endl;
	}
	char* GetDate()
	{
		return Date;
	}
	int GetQuantity()
	{
		return Quantity;
	}
	void Show()
	{
		cout << endl << "Invoice" << endl;
		cout << "�: " << Number << endl;
		cout << "Sold goods: " << Name << endl;
		cout << "Seller: " << Sign << endl;
		cout << "Quantity of goods (kg): " << Quantity << endl;	
		cout << "Date: " << Date << endl;
	}
protected:
	int Quantity;
	char Date[100];
};

class Document : public Print
{
public:
	Document(const char* name, const char * sign, int number = 0, int page = 0) : Print(name, sign, number)
	{
		Pages = page;
		cout << "Document constructor" << endl;
	}
	int GetPages()
	{
		return Pages;
	}
	void Show()
	{
		cout << endl << "Document" << endl;
		cout << "�: " << Number << endl;
		cout << "Title: " << Name << endl;
		cout << "Number of pages: " << Pages << endl;
		cout << "Sign : " << Sign << endl;	
	}
protected:
	int Pages;
};

class Check : public Print
{
public:
	Check(const char* name, const char * sign, const char * luckyboy, int number = 0, int sum = 0) : Print(name, sign, number, sum)
	{
		strcpy_s(Luckyboy, luckyboy);
		cout << "Check constructor" << endl;
	}
	char* GetForWhom()
	{
		return Luckyboy;
	}
	void Show()
	{
		cout << endl << "Check" << endl;
		cout << "�: " << Number << endl;
		cout << "For: " << Luckyboy << endl;
		cout << "Pay to the order of: " << Name << endl;
		cout << "Sum: " << Sum << endl;
		cout << "Sign: " << Sign << endl;			
	}
protected:
	char Luckyboy[100];
};

int main()
{
	Receipt * a = new Receipt("Speeding fine", "Musorsky A.S.", 1228488337, 5000);
	a->Show();
	Invoice * b = new Invoice("Snow", "Antony Montana", "10.07.1983", 5000, 420024);
	b->Show();
	Document * c = new Document("Stolen car Report", "Johnson C.J.", 19922991, 69);
	c->Show();
	Check * d = new Check("Lottery", "Navalny A.", "Kuts D.A.", 231231313, 99999);
	d->Show();
	system("pause");
	return 0;
}