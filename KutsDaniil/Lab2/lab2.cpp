#include <iostream>
#include <string.h>
using namespace std;

int main()
{
	char str[256], l = 0, r = 0;
	cout << "Enter the text:" << endl;
	cin.getline(str, 255);
	int len = strlen(str), buf = 1;
	cout << "All palindromes found in the text:" << endl;
	while (r != len + 1) {
		if (str[r] == ' ' || str[r] == '.') {
			int left = l, right = r - 1;
			while (left < right) {
				if (str[left] != str[right]) buf = 0; 
				left++;
				right--;
			}
			if (buf != 0) {
				for (int i = l; i < r; i++) cout << str[i];
				cout << endl;
			}
			l = r + 1;
		}
		buf = 1;
		r++;
	}
	system("pause");
	return 0;
}