#include <iostream>
#include <list>
using namespace std;

class ATD {
protected:
	virtual void showInf() = 0;		
};

class Process : public ATD {
protected:
public:
	list<char> l1 = { 'a','b','c','d','e' };
	void showInf() override {
		cout << "List: ";
		for (char n : l1) {
			cout << n << " ";		
		}
		cout << endl;		
	}
	char& operator [] (const int index);
	list<char>& operator +(const list<char> second) {
		auto iter2 = second.begin();
		for (char n : second) {
			l1.emplace_back(*iter2);
			++iter2;
		}
		return l1;
	}
	bool operator !=(const list<char> second) {
		auto iter = l1.begin();
		auto iter2 = second.begin();
		for (char n : second) {
			if (*iter == *iter2) {
				return false;
			}
		}
		return true;
	}
	int begin;		
};
char& Process::operator[](const int index) {
	auto iter = l1.begin();
	for (int i = 0; i < index; i++) {
		iter++;
	}
	return *iter;
}

int main() {
	Process List;
	list<char> l2 = { 'f','g','h','i','j' };
	cout << "First ";
	List.showInf();
	cout << "Second List: e f g h i" << endl;
	cout << "Adding List 1 to List 2: " << endl;
	List + l2;
	cout << "Final ";
	List.showInf();
	cout << "Comparing List 1 to List 2: ";
	if (List != l2) {
		cout << "Lists are not equal" << endl;	
	}
	else
		cout << "Lists are equal" << endl;
	cout << "Chosen element: " << List[8] << endl;
	system("pause");
	return 0;
}