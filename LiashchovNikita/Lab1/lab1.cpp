﻿#include "pch.h"
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	// Float testing
	float a_f = 100;
	float b_f = 0.001;
	float c_f = pow(a_f - b_f, 4);
	cout << "Float: (a-b)^4 = " << c_f << endl;
	float d_f = pow(a_f, 4) - 4 * pow(a_f, 3)*b_f;
	cout << "Float: (a^4 - 4a^3b) = " << d_f << endl;
	float e_f = 6 * pow(a_f, 2)*pow(b_f, 2) - 4 * a_f*pow(b_f, 3) + pow(b_f, 4);
	cout << "Float: (6a^2b^2 - 4ab^3 + b^4) = " << e_f << endl;
	float f_f = c_f - d_f;
	cout << "Float: (a-b)^4 - (a^4 - 4a^3b) = " << f_f << endl;
	float g_f = f_f / e_f;
	cout << "Float: ((a-b)^4 - (a^4 - 4a^3b))/(6a^2b^2 - 4ab^3 + b^4) = " << g_f << endl;

	// Double testing
	double a_d = 100;
	double b_d = 0.001;
	double c_d = pow(a_d - b_d, 4);
	cout << "Double: (a-b)^4 = " << c_d << endl;
	double d_d = pow(a_d, 4) - 4 * pow(a_d, 3)*b_d;
	cout << "Double: (a^4 - 4a^3b) = " << d_d << endl;
	double e_d = 6 * pow(a_d, 2)*pow(b_d, 2) - 4 * a_d*pow(b_d, 3) + pow(b_d, 4);
	cout << "Double: (6a^2b^2 - 4ab^3 + b^4) = " << e_d << endl;
	double f_d = c_d - d_d;
	cout << "Double: (a-b)^4 - (a^4 - 4a^3b) = " << f_d << endl;
	double g_d = f_d /e_d;
	cout << "Double: ((a-b)^4 - (a^4 - 4a^3b))/(6a^2b^2 - 4ab^3 + b^4) = " << g_d << endl;

	//n&m
	int n_in,m_in,n, m;
	cout << "Enter n: "; cin >> n_in;
	cout << "Enter m: "; cin >> m_in;
	/*Set values*/ n = n_in; m = m_in;
	int nm_1 = n++*m;
	cout << "1) n++*m = " << nm_1 << endl;
	/*Reset values*/ n = n_in; m = m_in;
	int nm_2 = n++<m;
	cout << "2) n++<m = " << nm_2 << endl;
	/*Reset values*/ m = m_in;
	int nm_3 = m-- >m;
	cout << "3) m-- >m = " << nm_3 << endl;
}

