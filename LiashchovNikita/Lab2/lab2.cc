#include "stdafx.h"
#include <iostream>
#include <string>
#include <string.h>

using namespace std;

char word_to_str(char *str, char *new_str_isnum, char *new_str_isletter, char *new_str_isbothint, int n_buf, int n);		// Func for take and convert word to str

int _tmain(int argc, _TCHAR* argv[])
{
	char str[256];					// Arr srting
	char new_str[256];				// Arr new srting
	char new_str_isnum[256] = {0};		// Arr string for nums
	char new_str_isletter[256] = {0};		// Arr srting for letters
	char new_str_isboth[256] = {0};		// Arr srting for both
	int k = strlen(str);			// Var string length
	int n = 0;						// Var counter for while
	int n_buf = 0;					// Var for prev space position
	cin.getline(str,k);
	while(n!=k) {
		if(str[n]==' ') {
			//cout << "Space founded, position n = " << n << "  n_buf =" << n_buf << endl;
			word_to_str(str,new_str_isnum,new_str_isletter,new_str_isboth,n_buf,n);
			n_buf = n+1;
		}
		if(str[n]=='\0') {
			//cout << "End founded, position n = " << n << "  n_buf =" << n_buf << endl;
			word_to_str(str,new_str_isnum,new_str_isletter,new_str_isboth,n_buf,n);
		}
		n++;
	}
		cout << new_str_isboth << new_str_isletter << new_str_isnum << endl;
	return 0;
	
}

char word_to_str(char *str, char *new_str_isnum, char *new_str_isletter, char *new_str_isboth, int n_buf, int n) {
	char word[256];
	int i = 0, q = 0;
	int term = n-n_buf;
	bool isnum = false, isboth = false, isletter = false;
	for(int k = n_buf;k<n;k++) { word[q] = str[k]; q++;}
	word[term] = '\0';
	for(int z = n_buf;z<n;z++) {
		if(isalnum(word[i])) {										//Check if symbol - numeric or letter
			if(isdigit(word[i])) {
				if(word[i+1]=='\0') {
					if(isalpha(word[i-1])) {		//Check if word include nums and letters
						isboth = true;
					}
				} else if (i==0) {
					if(isalpha(word[i+1])) {		//Check if word include nums and letters
						isboth = true;
					}
				} else {
					if(isalpha(word[i+1]) || isalpha(word[i-1])) {		//Check if word include nums and letters
						isboth = true;
					}
				}
				isnum = true;
			} else {
				if(isalpha(word[i])) {								//Check is symbol - letter
					isletter = true;
				}
			}
		}
		i++;
	}
	if(isboth == true) {
		new_str_isboth[n]='\0';
		new_str_isboth = strcat(new_str_isboth, word);
		new_str_isboth = strcat(new_str_isboth, " ");
		
	} else if(isletter == true) {
		new_str_isletter[n]='\0';
		new_str_isletter = strcat(new_str_isletter, word);
		new_str_isletter = strcat(new_str_isletter, " ");
		
	} else { 
		new_str_isnum[n]='\0';
		new_str_isnum = strcat(new_str_isnum, word);
		new_str_isnum = strcat(new_str_isnum, " ");
		

	}
	return 0;
}


